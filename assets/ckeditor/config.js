/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	// 
	
	config.protectedSource.push(/<a[^>]*><\/a>/g);


	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [		
		{ name: 'basicstyles', groups: [ 'basicstyles'] }
		//{ name: 'paragraph',   groups: [ '' ] }		
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Strike,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.entities = false;
	config.removePlugins = 'resize';

};

CKEDITOR.config.allowedContent = true; 
CKEDITOR.dtd.$removeEmpty[ 'a' ] = false;
