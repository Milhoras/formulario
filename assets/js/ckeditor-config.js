$(document).ready(function(){

  var wordCount_text_1 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -1,
      maxCharCount: 100
  }

  var wordCount_text_7 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -1,
      maxCharCount: 150
  }

  var wordCount_text_2 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -1,
      maxCharCount: 100
  }

  var wordCount_text_3 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -1,
      maxCharCount: 100
  }

  var wordCount_text_4 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -1,
      maxCharCount: 200
  }

  var wordCount_text_5 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -1,
      maxCharCount: 100
  }

  var wordCount_text_6 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false, 
      maxWordCount: -1,
      maxCharCount: 50
  }

    CKEDITOR.replace( 'text_1' , {wordcount: wordCount_text_1});    
    CKEDITOR.replace( 'text_7' , {wordcount: wordCount_text_7});  
    CKEDITOR.replace( 'text_2', {wordcount: wordCount_text_2});   
    CKEDITOR.replace( 'text_3', {wordcount: wordCount_text_3} );   
    CKEDITOR.replace( 'text_4', {wordcount: wordCount_text_4} );   
    CKEDITOR.replace( 'text_5', {wordcount: wordCount_text_5} );   
    CKEDITOR.replace( 'text_6', {wordcount: wordCount_text_6} );

});