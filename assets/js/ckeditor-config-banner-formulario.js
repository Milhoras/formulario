$(document).ready(function(){

  var wordCount_text_1 = {
      showParagraphs: false,
      showWordCount: true,
      showCharCount: true,
      countSpacesAsChars: false,
      countHTML: false,
      maxWordCount: -10,
      maxCharCount: 50
  }


    CKEDITOR.replace( 'text_1' , {wordcount: wordCount_text_1});    

});