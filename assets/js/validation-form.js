function validar_login() {
    var password = $("#password");
    var email = $("#email");  
    $(".error").remove();

    if (is_empty(email.val())) {
        email.focus().after("<span class='error'>Email es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (!is_email(email.val())) {
        email.focus().after("<span class='error'>El email no es correcto</span>");
        hide_error()
        return false;
    }   

    if (is_empty(password.val())) {
        password.focus().after("<span class='error'>password es un campo obligatorio</span>");
        hide_error()
        return false;
    }

}

function validar_olvido_contrasenia()
{
    var email = $("#email");  
    $(".error").remove();

    if (is_empty(email.val())) {
        email.focus().after("<span class='error'>Email es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (!is_email(email.val())) {
        email.focus().after("<span class='error'>El email no es correcto</span>");
        hide_error()
        return false;
    } 
}

function validar_cambiar_contrasenia()
{
    var nueva_contrasenia = $("#nueva_contrasenia");
    var repetir_nueva_contrasenia = $("#repetir_nueva_contrasenia");

    var email = $("#email");  
    $(".error").remove();

    if(!(nueva_contrasenia.val() == repetir_nueva_contrasenia.val()))
    {
        nueva_contrasenia.focus().after("<span class='error'>Las contraseñas no coinciden</span>");
        hide_error()
        return false; 
    }

    if (is_empty(nueva_contrasenia.val())) {
        nueva_contrasenia.focus().after("<span class='error'>password es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(repetir_nueva_contrasenia.val())) {
        repetir_nueva_contrasenia.focus().after("<span class='error'>Debe repetir la nueva contraseña, es un campo obligatorio</span>");
        hide_error()
        return false;
    }
    
    if (is_empty(email.val())) {
        email.focus().after("<span class='error'>Email es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (!is_email(email.val())) {
        email.focus().after("<span class='error'>El email no es correcto</span>");
        hide_error()
        return false;
    }   
}

function validar_dni()
{
    var dni = $("#dni");
    $(".error").remove();

    if (is_empty(dni.val())) {
        dni.focus().after("<span class='error'>DNI es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (!is_valid_number(dni.val())) {
        dni.focus().after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        hide_error()
        return false;
    } else if ( count_caracter(dni.val())!=8){
         dni.focus().after("<span class='error'>La cantidad de digitos debe de ser 8</span>");
         hide_error()
        return false;
    }
}

function validar_registro_ganador()
{
    var nombre = $("#nombres");
    var apellidos = $("#apellidos");
    var dni = $("#dni");
    var email = $("#email");
    var ubigeo = $("#ubigeo");
    var telefono = $("#telefono");
    var direccion = $("#direccion"); 
    var error = $("#error");
    var error_ubigeo = $("#error_ubigeo");

    $(".error").remove();
    

    if (is_empty(nombre.val())) {
        nombre.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }
    else if (caracter_special(nombre.val())) {
        nombre.focus().after("<span class='error'>No usar caracteres especiales</span>");
        hide_error()
        return false;
    }
    else if (!only_letter(nombre.val())) {
        nombre.focus().after("<span class='error'>S&oacute;lo pueden ser letras</span>");
        hide_error()
        return false;
    }

    if (is_empty(apellidos.val())) {
        apellidos.focus().after("<span class='error'>Apellido es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (caracter_special(apellidos.val())) {
        apellidos.focus().after("<span class='error'>No usar caracteres especiales</span>");
        hide_error()
        return false;
    } else if (!only_letter(apellidos.val())) {
        apellidos.focus().after("<span class='error'>S&oacute;lo pueden ser letras</span>");
        hide_error()
        return false;
    }


    if (is_empty(email.val())) {
        email.focus().after("<span class='error'>Email es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (!is_email(email.val())) {
        email.focus().after("<span class='error'>El email no es correcto</span>");
        hide_error()
        return false;
    }

    if (is_empty(dni.val())) {
        dni.focus().after("<span class='error'>DNI es un campo obligatorio</span>");
        hide_error()
        return false;
    } else if (!is_valid_number(dni.val())) {
        dni.focus().after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        hide_error()
        return false;
    } else if ( count_caracter(dni.val())!=8){
         dni.focus().after("<span class='error'>La cantidad de digitos debe de ser 8</span>");
         hide_error()
        return false;
    }
    
    if (ubigeo.val() =="") {
        error_ubigeo.focus().after("<span class='error'>Debe elegir una opci&oacute;n</span>");
        return false;
    }
    
    if (is_empty(telefono.val())) {
        telefono.focus().after("<span class='error'>Tel&eacute;fono es un campo obligatorio</span>");
        hide_error()
        return false;

    } else if (!is_valid_number(telefono.val())) {
        telefono.focus().after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        hide_error()
        return false;

    }  else if (count_caracter(telefono.val())< 7  ) {
        telefono.focus().after("<span class='error'>N&uacute;mero incompleto</span>");
        hide_error()
        return false;
    } else if (count_caracter(telefono.val())> 9 ) {
        telefono.focus().after("<span class='error'>N&uacute;mero demasiado largo</span>");
        hide_error()
        return false;
    } 

    if (is_empty(direccion.val())) {
        direccion.focus().after("<span class='error'>Dirección es un campo obligatorio</span>");
        hide_error()
        return false;

    }
    

}

function validar_registro_campania()
{
    var nombre = $("#nombre");
    var fecha_inicio = $("#fecha_inicio");
    var fecha_fin = $("#fecha_fin");
    var id_radio = $("#id_radio");

    $(".error").remove();    

    if (is_empty(nombre.val())) {
        nombre.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(id_radio.val())) {
        id_radio.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(fecha_inicio.val())) {
        fecha_inicio.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(fecha_fin.val())) {
        fecha_fin.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if(fecha_inicio.val() > fecha_fin.val())
    {
        fecha_fin.focus().after("<span class='error'>La fecha de fin debe ser mayor a la de inicio</span>");
        hide_error()
        return false;
    }

}

function validar_registro_mecanica()
{
    var url = $("#url");
    var fecha_inicio = $("#fecha_inicio");
    var fecha_fin = $("#fecha_fin");
    var cantidad_ganadores = $("#cantidad_ganadores");
    var id_tipo_mecanica = $("#id_tipo_mecanica");
    var id_sub_tipo_mecanica = $("#id_sub_tipo_mecanica");
    var error_departamento = $("#error_departamento");
    

    $(".error").remove();    
    var bool_cantidad = valDepartamentos();

    if (is_empty(url.val())) {
        url.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(fecha_inicio.val())) {
        fecha_inicio.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(fecha_fin.val())) {
        fecha_fin.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if (is_empty(id_tipo_mecanica.val())) {
        id_tipo_mecanica.focus().after("<span class='error'>Es un campo obligatorio</span>");
        hide_error()
        return false;
    }

    if(id_tipo_mecanica.val() != 4)
    {
        if (is_empty(id_sub_tipo_mecanica.val())) {
            id_sub_tipo_mecanica.focus().after("<span class='error'>Es un campo obligatorio</span>");
            hide_error()
            return false;
        }
    }

   switch(id_tipo_mecanica.val())
    {
        case '2':
        var id_tipo_cabecera = $("#id_tipo_cabecera");
        var fecha_sorteo = $("#fecha_sorteo");
        var hora_sorteo = $("#hora_sorteo");
        var seo_title = $("#seo_title");

        if(id_sub_tipo_mecanica == '7')
        {
            if (is_empty(id_tipo_cabecera.val())) {
            id_tipo_cabecera.focus().after("<span class='error'>Es un campo obligatorio</span>");
            hide_error()
            return false;
            }
            else if (id_tipo_cabecera.val() == 0) {
                id_tipo_cabecera.focus().after("<span class='error'>Es un campo obligatorio</span>");
                hide_error()
                return false;
            }
        }
        

        if (is_empty(fecha_sorteo.val())) {
            fecha_sorteo.focus().after("<span class='error'>Es un campo obligatorio</span>");
            hide_error()
            return false;
        }
        else if (fecha_sorteo.val() == '0000-00-00') {
            fecha_sorteo.focus().after("<span class='error'>Es un campo obligatorio</span>");
            hide_error()
            return false;
        }

        if (is_empty(hora_sorteo.val())) {
            hora_sorteo.focus().after("<span class='error'>Es un campo obligatorio</span>");
            hide_error()
            return false;
        }

        if (is_empty(seo_title.val())) {
            seo_title.focus().after("<span class='error'>Es un campo obligatorio</span>");
            hide_error()
            return false;
        }
        
        if(bool_cantidad == 0)
        {
            error_departamento.after("<span class='error'>Debe elegir al menos 1 departamento</span>");
            hide_error();
            return false;
        }

        break;
    }

    

    if(fecha_inicio.val() > fecha_fin.val())
    {
        fecha_fin.focus().after("<span class='error'>La fecha de fin debe ser mayor a la de inicio</span>");
        hide_error()
        return false;
    }

    if (is_empty(cantidad_ganadores.val())) {
        cantidad_ganadores.focus().after("<span class='error'>Nombre es un campo obligatorio</span>");
        hide_error()
        return false;
    }
    else if (!is_valid_number(cantidad_ganadores.val())) {
        cantidad_ganadores.focus().after("<span class='error'>S&oacute;lo pueden ser n&uacute;meros</span>");
        hide_error()
        return false;
    }
    
}

// Validación para los campos SEO y FB
function validar_tipo_mecanica(id_tipo_mecanica)
{
    //console.log("id_tipo_mecanica: "+id_tipo_mecanica);
    $("#div_banner_formulario").hide();
    $("#div_form_minisites").hide();
}

function validar_sub_tipo_mecanica(id_tipo_mecanica, id_sub_tipo_mecanica)
{
    //console.log("id_tipo_mecanica: "+id_tipo_mecanica);
    //console.log("id_sub_tipo_mecanica: "+id_sub_tipo_mecanica);

    switch(id_tipo_mecanica)
    {
        case '1':// Formularios
            switch(id_sub_tipo_mecanica)
            {
                case '1':
                    $("#div_banner_formulario").hide();
                    $("#div_form_minisites").show();
                break;

                default:
                    $("#div_banner_formulario").show();
                    $("#div_form_minisites").hide();
                break;
            }

        break;

        case '2': // Minisites
            $("#div_banner_formulario").hide();
            $("#div_form_minisites").show();
        break;

        default:
            $("#div_banner_formulario").hide();
            $("#div_form_minisites").hide();
        break;
    }
  
}

function validar_puntaje_adicional()
{
    var puntaje_real = parseInt($("#puntaje_real").val());
    var puntaje_adicional = parseInt($("#puntaje_adicional").val());
    var puntaje_total = parseInt($("#puntaje_total").val());
    var puntaje_web = parseInt(puntaje_adicional + puntaje_real);
    $(".error").remove();    

    console.log(puntaje_web);
    if (puntaje_total > puntaje_web) {
        $("#puntaje_adicional").focus().after("<span class='error'>El nuevo puntaje no puede ser menor a lo actual</span>");
        hide_error()
        return false;
    }

}

function valDepartamentos()
{
    var cantidad = 0;

    $("input[name*='departamentos']").each(function(){

        if($(this).is(':checked'))
        {
            cantidad++;
        }
    });

    return cantidad;
}

function validar_registro_matriz()
{
    var file_name = $("#file_name");
    var id_tipo_matriz = $("#id_tipo_matriz");
    $(".error").remove();  

    if (is_empty(file_name.val())) {
        file_name.focus().after("<span class='error error-matriz'>Debe elegir un archivo Excel</span>");
        hide_error()
        return false;
    }

    if (is_empty(id_tipo_matriz.val())) {
        id_tipo_matriz.focus().after("<span class='error'>Es un campo obligatorio</span>");
        hide_error()
        return false;
    }
    else if (id_tipo_matriz.val() == 0) {
        id_tipo_matriz.focus().after("<span class='error'>Es un campo obligatorio</span>");
        hide_error()
        return false;
    }

}
