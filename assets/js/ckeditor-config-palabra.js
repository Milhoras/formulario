$(document).ready(function(){

    var wordCount_text_1 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 180
    }
  
    var wordCount_text_2 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 100
    }
  
    var wordCount_text_3 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 100
    }

    var wordCount_text_4 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 280
    }

    var wordCount_text_5 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 200
    }

    var wordCount_text_6 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 260
    }

    var wordCount_text_7 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 180
    }

    var wordCount_text_8 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 180
    }

    var wordCount_text_9 = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 180
    }

    var wordCount_Limite = {
        showParagraphs: false,
        showWordCount: true,
        showCharCount: true,
        countSpacesAsChars: false,
        countHTML: false,
        maxWordCount: -1,
        maxCharCount: 180
    }
  
      CKEDITOR.replace( 'text_1' , {wordcount: wordCount_text_1});    
      CKEDITOR.replace( 'text_2' , {wordcount: wordCount_text_2});  
      CKEDITOR.replace( 'text_3', {wordcount: wordCount_text_3});   
      CKEDITOR.replace( 'text_4', {wordcount: wordCount_text_4} );
      CKEDITOR.replace( 'text_5', {wordcount: wordCount_text_5} );
      CKEDITOR.replace( 'text_6', {wordcount: wordCount_text_6} );
      CKEDITOR.replace( 'text_7', {wordcount: wordCount_text_7} );
      CKEDITOR.replace( 'text_8', {wordcount: wordCount_text_8} );
      CKEDITOR.replace( 'text_9', {wordcount: wordCount_text_9} );
      CKEDITOR.replace( 'text_10', {wordcount: wordCount_Limite} );
  
  });