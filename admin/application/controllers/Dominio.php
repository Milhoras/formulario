<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Dominio extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Dominio_model');
    } 

    /*
     * Listing of dominios
     */
    function index()
    {
        $data['dominios'] = $this->Dominio_model->get_all_dominios();
        
        $data['_view'] = 'dominio/index';
        $this->load->view('layouts/main',$data);
    }

    /*
     * Adding a new dominio
     */
    function add()
    {   
        if(isset($_POST) && count($_POST) > 0)     
        {   
            $params = array(
				'dominio' => $this->input->post('dominio'),
				'nombre' => $this->input->post('nombre'),
				'tld' => $this->input->post('tld'),
				'numero' => $this->input->post('numero'),
				'ruc_dni' => $this->input->post('ruc_dni'),
				'razon_social' => $this->input->post('razon_social'),
				'tipo_razon_nombre' => $this->input->post('tipo_razon_nombre'),
				'email' => $this->input->post('email'),
				'telefono' => $this->input->post('telefono'),
				'direccion' => $this->input->post('direccion'),
				'id_departamento' => $this->input->post('id_departamento'),
				'id_provincia' => $this->input->post('id_provincia'),
				'id_distrito' => $this->input->post('id_distrito'),
				'id_negocio' => $this->input->post('id_negocio'),
            );
            
            $dominio_id = $this->Dominio_model->add_dominio($params);
            redirect('dominio/index');
        }
        else
        {            
            $data['_view'] = 'dominio/add';
            $this->load->view('layouts/main',$data);
        }
    }  

    /*
     * Editing a dominio
     */
    function edit($id_dominio)
    {   
        // check if the dominio exists before trying to edit it
        $data['dominio'] = $this->Dominio_model->get_dominio($id_dominio);
        
        if(isset($data['dominio']['id_dominio']))
        {
            if(isset($_POST) && count($_POST) > 0)     
            {   
                $params = array(
					'dominio' => $this->input->post('dominio'),
					'nombre' => $this->input->post('nombre'),
					'tld' => $this->input->post('tld'),
					'numero' => $this->input->post('numero'),
					'ruc_dni' => $this->input->post('ruc_dni'),
					'razon_social' => $this->input->post('razon_social'),
					'tipo_razon_nombre' => $this->input->post('tipo_razon_nombre'),
					'email' => $this->input->post('email'),
					'telefono' => $this->input->post('telefono'),
					'direccion' => $this->input->post('direccion'),
					'id_departamento' => $this->input->post('id_departamento'),
					'id_provincia' => $this->input->post('id_provincia'),
					'id_distrito' => $this->input->post('id_distrito'),
					'id_negocio' => $this->input->post('id_negocio'),
                );

                $this->Dominio_model->update_dominio($id_dominio,$params);            
                redirect('dominio/index');
            }
            else
            {
                $data['_view'] = 'dominio/edit';
                $this->load->view('layouts/main',$data);
            }
        }
        else
            show_error('The dominio you are trying to edit does not exist.');
    } 

    /*
     * Deleting dominio
     */
    function remove($id_dominio)
    {
        $dominio = $this->Dominio_model->get_dominio($id_dominio);

        // check if the dominio exists before trying to delete it
        if(isset($dominio['id_dominio']))
        {
            $this->Dominio_model->delete_dominio($id_dominio);
            redirect('dominio/index');
        }
        else
            show_error('The dominio you are trying to delete does not exist.');
    }
    
}
