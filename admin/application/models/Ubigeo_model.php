<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Ubigeo_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get ubigeo by id_ubigeo
     */
    function get_ubigeo($id_ubigeo)
    {
        return $this->db->get_where('ubigeos',array('id_ubigeo'=>$id_ubigeo))->row_array();
    }
        
    /*
     * Get all ubigeos
     */
    function get_all_ubigeos()
    {
        $this->db->order_by('id_ubigeo', 'desc');
        return $this->db->get('ubigeos')->result_array();
    }
        
    /*
     * function to add new ubigeo
     */
    function add_ubigeo($params)
    {
        $this->db->insert('ubigeos',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update ubigeo
     */
    function update_ubigeo($id_ubigeo,$params)
    {
        $this->db->where('id_ubigeo',$id_ubigeo);
        return $this->db->update('ubigeos',$params);
    }
    
    /*
     * function to delete ubigeo
     */
    function delete_ubigeo($id_ubigeo)
    {
        return $this->db->delete('ubigeos',array('id_ubigeo'=>$id_ubigeo));
    }
}
