<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Plantilla Edit</h3>
            </div>
			<?php echo form_open('plantilla/edit/'.$plantilla['id_plantilla']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="grupo" class="control-label">Grupo</label>
						<div class="form-group">
							<input type="text" name="grupo" value="<?php echo ($this->input->post('grupo') ? $this->input->post('grupo') : $plantilla['grupo']); ?>" class="form-control" id="grupo" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $plantilla['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="imagen" class="control-label">Imagen</label>
						<div class="form-group">
							<input type="text" name="imagen" value="<?php echo ($this->input->post('imagen') ? $this->input->post('imagen') : $plantilla['imagen']); ?>" class="form-control" id="imagen" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>