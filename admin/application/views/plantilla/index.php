<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Plantillas Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('plantilla/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Plantilla</th>
						<th>Grupo</th>
						<th>Nombre</th>
						<th>Imagen</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($plantillas as $p){ ?>
                    <tr>
						<td><?php echo $p['id_plantilla']; ?></td>
						<td><?php echo $p['grupo']; ?></td>
						<td><?php echo $p['nombre']; ?></td>
						<td><?php echo $p['imagen']; ?></td>
						<td>
                            <a href="<?php echo site_url('plantilla/edit/'.$p['id_plantilla']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('plantilla/remove/'.$p['id_plantilla']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
