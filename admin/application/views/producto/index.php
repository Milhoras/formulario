<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Productos Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('producto/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Producto</th>
						<th>Id Negocio</th>
						<th>Nombre</th>
						<th>Codigo</th>
						<th>Categoria</th>
						<th>Precio</th>
						<th>Estado</th>
						<th>Descripcion</th>
						<th>Imagen</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($productos as $p){ ?>
                    <tr>
						<td><?php echo $p['id_producto']; ?></td>
						<td><?php echo $p['id_negocio']; ?></td>
						<td><?php echo $p['nombre']; ?></td>
						<td><?php echo $p['codigo']; ?></td>
						<td><?php echo $p['categoria']; ?></td>
						<td><?php echo $p['precio']; ?></td>
						<td><?php echo $p['estado']; ?></td>
						<td><?php echo $p['descripcion']; ?></td>
						<td><?php echo $p['imagen']; ?></td>
						<td>
                            <a href="<?php echo site_url('producto/edit/'.$p['id_producto']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('producto/remove/'.$p['id_producto']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
