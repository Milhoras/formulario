<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Producto Edit</h3>
            </div>
			<?php echo form_open('producto/edit/'.$producto['id_producto']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="id_negocio" class="control-label">Id Negocio</label>
						<div class="form-group">
							<input type="text" name="id_negocio" value="<?php echo ($this->input->post('id_negocio') ? $this->input->post('id_negocio') : $producto['id_negocio']); ?>" class="form-control" id="id_negocio" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $producto['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="codigo" class="control-label">Codigo</label>
						<div class="form-group">
							<input type="text" name="codigo" value="<?php echo ($this->input->post('codigo') ? $this->input->post('codigo') : $producto['codigo']); ?>" class="form-control" id="codigo" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="categoria" class="control-label">Categoria</label>
						<div class="form-group">
							<input type="text" name="categoria" value="<?php echo ($this->input->post('categoria') ? $this->input->post('categoria') : $producto['categoria']); ?>" class="form-control" id="categoria" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="precio" class="control-label">Precio</label>
						<div class="form-group">
							<input type="text" name="precio" value="<?php echo ($this->input->post('precio') ? $this->input->post('precio') : $producto['precio']); ?>" class="form-control" id="precio" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="estado" class="control-label">Estado</label>
						<div class="form-group">
							<input type="text" name="estado" value="<?php echo ($this->input->post('estado') ? $this->input->post('estado') : $producto['estado']); ?>" class="form-control" id="estado" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="descripcion" class="control-label">Descripcion</label>
						<div class="form-group">
							<textarea name="descripcion" class="form-control" id="descripcion"><?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $producto['descripcion']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="imagen" class="control-label">Imagen</label>
						<div class="form-group">
							<textarea name="imagen" class="form-control" id="imagen"><?php echo ($this->input->post('imagen') ? $this->input->post('imagen') : $producto['imagen']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>