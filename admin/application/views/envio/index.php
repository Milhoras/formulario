<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Envios Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('envio/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Envio</th>
						<th>Id Departamento</th>
						<th>Id Provincia</th>
						<th>Id Distrito</th>
						<th>Id Negocio</th>
						<th>Costo</th>
						<th>Tiempo</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($envios as $e){ ?>
                    <tr>
						<td><?php echo $e['id_envio']; ?></td>
						<td><?php echo $e['id_departamento']; ?></td>
						<td><?php echo $e['id_provincia']; ?></td>
						<td><?php echo $e['id_distrito']; ?></td>
						<td><?php echo $e['id_negocio']; ?></td>
						<td><?php echo $e['costo']; ?></td>
						<td><?php echo $e['tiempo']; ?></td>
						<td>
                            <a href="<?php echo site_url('envio/edit/'.$e['id_envio']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('envio/remove/'.$e['id_envio']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
