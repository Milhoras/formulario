<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Envio Add</h3>
            </div>
            <?php echo form_open('envio/add'); ?>
          	<div class="box-body">
          		<div class="row clearfix">
					<div class="col-md-6">
						<label for="id_departamento" class="control-label">Id Departamento</label>
						<div class="form-group">
							<input type="text" name="id_departamento" value="<?php echo $this->input->post('id_departamento'); ?>" class="form-control" id="id_departamento" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_provincia" class="control-label">Id Provincia</label>
						<div class="form-group">
							<input type="text" name="id_provincia" value="<?php echo $this->input->post('id_provincia'); ?>" class="form-control" id="id_provincia" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_distrito" class="control-label">Id Distrito</label>
						<div class="form-group">
							<input type="text" name="id_distrito" value="<?php echo $this->input->post('id_distrito'); ?>" class="form-control" id="id_distrito" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_negocio" class="control-label">Id Negocio</label>
						<div class="form-group">
							<input type="text" name="id_negocio" value="<?php echo $this->input->post('id_negocio'); ?>" class="form-control" id="id_negocio" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="costo" class="control-label">Costo</label>
						<div class="form-group">
							<input type="text" name="costo" value="<?php echo $this->input->post('costo'); ?>" class="form-control" id="costo" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tiempo" class="control-label">Tiempo</label>
						<div class="form-group">
							<input type="text" name="tiempo" value="<?php echo $this->input->post('tiempo'); ?>" class="form-control" id="tiempo" />
						</div>
					</div>
				</div>
			</div>
          	<div class="box-footer">
            	<button type="submit" class="btn btn-success">
            		<i class="fa fa-check"></i> Save
            	</button>
          	</div>
            <?php echo form_close(); ?>
      	</div>
    </div>
</div>