<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Dominios Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('dominio/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Dominio</th>
						<th>Dominio</th>
						<th>Nombre</th>
						<th>Tld</th>
						<th>Numero</th>
						<th>Ruc Dni</th>
						<th>Razon Social</th>
						<th>Tipo Razon Nombre</th>
						<th>Email</th>
						<th>Telefono</th>
						<th>Direccion</th>
						<th>Id Departamento</th>
						<th>Id Provincia</th>
						<th>Id Distrito</th>
						<th>Id Negocio</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($dominios as $d){ ?>
                    <tr>
						<td><?php echo $d['id_dominio']; ?></td>
						<td><?php echo $d['dominio']; ?></td>
						<td><?php echo $d['nombre']; ?></td>
						<td><?php echo $d['tld']; ?></td>
						<td><?php echo $d['numero']; ?></td>
						<td><?php echo $d['ruc_dni']; ?></td>
						<td><?php echo $d['razon_social']; ?></td>
						<td><?php echo $d['tipo_razon_nombre']; ?></td>
						<td><?php echo $d['email']; ?></td>
						<td><?php echo $d['telefono']; ?></td>
						<td><?php echo $d['direccion']; ?></td>
						<td><?php echo $d['id_departamento']; ?></td>
						<td><?php echo $d['id_provincia']; ?></td>
						<td><?php echo $d['id_distrito']; ?></td>
						<td><?php echo $d['id_negocio']; ?></td>
						<td>
                            <a href="<?php echo site_url('dominio/edit/'.$d['id_dominio']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('dominio/remove/'.$d['id_dominio']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
