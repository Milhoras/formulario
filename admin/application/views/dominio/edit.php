<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Dominio Edit</h3>
            </div>
			<?php echo form_open('dominio/edit/'.$dominio['id_dominio']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="dominio" class="control-label">Dominio</label>
						<div class="form-group">
							<input type="text" name="dominio" value="<?php echo ($this->input->post('dominio') ? $this->input->post('dominio') : $dominio['dominio']); ?>" class="form-control" id="dominio" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $dominio['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tld" class="control-label">Tld</label>
						<div class="form-group">
							<input type="text" name="tld" value="<?php echo ($this->input->post('tld') ? $this->input->post('tld') : $dominio['tld']); ?>" class="form-control" id="tld" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="numero" class="control-label">Numero</label>
						<div class="form-group">
							<input type="text" name="numero" value="<?php echo ($this->input->post('numero') ? $this->input->post('numero') : $dominio['numero']); ?>" class="form-control" id="numero" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="ruc_dni" class="control-label">Ruc Dni</label>
						<div class="form-group">
							<input type="text" name="ruc_dni" value="<?php echo ($this->input->post('ruc_dni') ? $this->input->post('ruc_dni') : $dominio['ruc_dni']); ?>" class="form-control" id="ruc_dni" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="razon_social" class="control-label">Razon Social</label>
						<div class="form-group">
							<input type="text" name="razon_social" value="<?php echo ($this->input->post('razon_social') ? $this->input->post('razon_social') : $dominio['razon_social']); ?>" class="form-control" id="razon_social" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="tipo_razon_nombre" class="control-label">Tipo Razon Nombre</label>
						<div class="form-group">
							<input type="text" name="tipo_razon_nombre" value="<?php echo ($this->input->post('tipo_razon_nombre') ? $this->input->post('tipo_razon_nombre') : $dominio['tipo_razon_nombre']); ?>" class="form-control" id="tipo_razon_nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $dominio['email']); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="telefono" class="control-label">Telefono</label>
						<div class="form-group">
							<input type="text" name="telefono" value="<?php echo ($this->input->post('telefono') ? $this->input->post('telefono') : $dominio['telefono']); ?>" class="form-control" id="telefono" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="direccion" class="control-label">Direccion</label>
						<div class="form-group">
							<input type="text" name="direccion" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $dominio['direccion']); ?>" class="form-control" id="direccion" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_departamento" class="control-label">Id Departamento</label>
						<div class="form-group">
							<input type="text" name="id_departamento" value="<?php echo ($this->input->post('id_departamento') ? $this->input->post('id_departamento') : $dominio['id_departamento']); ?>" class="form-control" id="id_departamento" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_provincia" class="control-label">Id Provincia</label>
						<div class="form-group">
							<input type="text" name="id_provincia" value="<?php echo ($this->input->post('id_provincia') ? $this->input->post('id_provincia') : $dominio['id_provincia']); ?>" class="form-control" id="id_provincia" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_distrito" class="control-label">Id Distrito</label>
						<div class="form-group">
							<input type="text" name="id_distrito" value="<?php echo ($this->input->post('id_distrito') ? $this->input->post('id_distrito') : $dominio['id_distrito']); ?>" class="form-control" id="id_distrito" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_negocio" class="control-label">Id Negocio</label>
						<div class="form-group">
							<input type="text" name="id_negocio" value="<?php echo ($this->input->post('id_negocio') ? $this->input->post('id_negocio') : $dominio['id_negocio']); ?>" class="form-control" id="id_negocio" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>