<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Negocios Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('negocio/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Negocio</th>
						<th>Ruc</th>
						<th>Nombre</th>
						<th>Telefono</th>
						<th>Email</th>
						<th>Local</th>
						<th>Direccion</th>
						<th>Id Departamento</th>
						<th>Id Provincia</th>
						<th>Id Distrito</th>
						<th>Numero Cuenta</th>
						<th>Numero Cci</th>
						<th>Id Banco</th>
						<th>Id Plantilla</th>
						<th>Fecha Registro</th>
						<th>Fecha Modificacion</th>
						<th>Estado</th>
						<th>Redsocial Facebook</th>
						<th>Redsocial Twitter</th>
						<th>Redsocial Instagram</th>
						<th>Redsocial Youtube</th>
						<th>Logo</th>
						<th>Imagen Banner</th>
						<th>Texto Banner</th>
						<th>Descripcion</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($negocios as $n){ ?>
                    <tr>
						<td><?php echo $n['id_negocio']; ?></td>
						<td><?php echo $n['ruc']; ?></td>
						<td><?php echo $n['nombre']; ?></td>
						<td><?php echo $n['telefono']; ?></td>
						<td><?php echo $n['email']; ?></td>
						<td><?php echo $n['local']; ?></td>
						<td><?php echo $n['direccion']; ?></td>
						<td><?php echo $n['id_departamento']; ?></td>
						<td><?php echo $n['id_provincia']; ?></td>
						<td><?php echo $n['id_distrito']; ?></td>
						<td><?php echo $n['numero_cuenta']; ?></td>
						<td><?php echo $n['numero_cci']; ?></td>
						<td><?php echo $n['id_banco']; ?></td>
						<td><?php echo $n['id_plantilla']; ?></td>
						<td><?php echo $n['fecha_registro']; ?></td>
						<td><?php echo $n['fecha_modificacion']; ?></td>
						<td><?php echo $n['estado']; ?></td>
						<td><?php echo $n['redsocial_facebook']; ?></td>
						<td><?php echo $n['redsocial_twitter']; ?></td>
						<td><?php echo $n['redsocial_instagram']; ?></td>
						<td><?php echo $n['redsocial_youtube']; ?></td>
						<td><?php echo $n['logo']; ?></td>
						<td><?php echo $n['imagen_banner']; ?></td>
						<td><?php echo $n['texto_banner']; ?></td>
						<td><?php echo $n['descripcion']; ?></td>
						<td>
                            <a href="<?php echo site_url('negocio/edit/'.$n['id_negocio']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('negocio/remove/'.$n['id_negocio']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
