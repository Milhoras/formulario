<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Negocio Edit</h3>
            </div>
			<?php echo form_open('negocio/edit/'.$negocio['id_negocio']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="ruc" class="control-label">Ruc</label>
						<div class="form-group">
							<input type="text" name="ruc" value="<?php echo ($this->input->post('ruc') ? $this->input->post('ruc') : $negocio['ruc']); ?>" class="form-control" id="ruc" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $negocio['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="telefono" class="control-label">Telefono</label>
						<div class="form-group">
							<input type="text" name="telefono" value="<?php echo ($this->input->post('telefono') ? $this->input->post('telefono') : $negocio['telefono']); ?>" class="form-control" id="telefono" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="email" class="control-label">Email</label>
						<div class="form-group">
							<input type="text" name="email" value="<?php echo ($this->input->post('email') ? $this->input->post('email') : $negocio['email']); ?>" class="form-control" id="email" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="local" class="control-label">Local</label>
						<div class="form-group">
							<input type="text" name="local" value="<?php echo ($this->input->post('local') ? $this->input->post('local') : $negocio['local']); ?>" class="form-control" id="local" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="direccion" class="control-label">Direccion</label>
						<div class="form-group">
							<input type="text" name="direccion" value="<?php echo ($this->input->post('direccion') ? $this->input->post('direccion') : $negocio['direccion']); ?>" class="form-control" id="direccion" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_departamento" class="control-label">Id Departamento</label>
						<div class="form-group">
							<input type="text" name="id_departamento" value="<?php echo ($this->input->post('id_departamento') ? $this->input->post('id_departamento') : $negocio['id_departamento']); ?>" class="form-control" id="id_departamento" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_provincia" class="control-label">Id Provincia</label>
						<div class="form-group">
							<input type="text" name="id_provincia" value="<?php echo ($this->input->post('id_provincia') ? $this->input->post('id_provincia') : $negocio['id_provincia']); ?>" class="form-control" id="id_provincia" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_distrito" class="control-label">Id Distrito</label>
						<div class="form-group">
							<input type="text" name="id_distrito" value="<?php echo ($this->input->post('id_distrito') ? $this->input->post('id_distrito') : $negocio['id_distrito']); ?>" class="form-control" id="id_distrito" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="numero_cuenta" class="control-label">Numero Cuenta</label>
						<div class="form-group">
							<input type="text" name="numero_cuenta" value="<?php echo ($this->input->post('numero_cuenta') ? $this->input->post('numero_cuenta') : $negocio['numero_cuenta']); ?>" class="form-control" id="numero_cuenta" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="numero_cci" class="control-label">Numero Cci</label>
						<div class="form-group">
							<input type="text" name="numero_cci" value="<?php echo ($this->input->post('numero_cci') ? $this->input->post('numero_cci') : $negocio['numero_cci']); ?>" class="form-control" id="numero_cci" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_banco" class="control-label">Id Banco</label>
						<div class="form-group">
							<input type="text" name="id_banco" value="<?php echo ($this->input->post('id_banco') ? $this->input->post('id_banco') : $negocio['id_banco']); ?>" class="form-control" id="id_banco" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_plantilla" class="control-label">Id Plantilla</label>
						<div class="form-group">
							<input type="text" name="id_plantilla" value="<?php echo ($this->input->post('id_plantilla') ? $this->input->post('id_plantilla') : $negocio['id_plantilla']); ?>" class="form-control" id="id_plantilla" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="fecha_registro" class="control-label">Fecha Registro</label>
						<div class="form-group">
							<input type="text" name="fecha_registro" value="<?php echo ($this->input->post('fecha_registro') ? $this->input->post('fecha_registro') : $negocio['fecha_registro']); ?>" class="form-control" id="fecha_registro" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="fecha_modificacion" class="control-label">Fecha Modificacion</label>
						<div class="form-group">
							<input type="text" name="fecha_modificacion" value="<?php echo ($this->input->post('fecha_modificacion') ? $this->input->post('fecha_modificacion') : $negocio['fecha_modificacion']); ?>" class="form-control" id="fecha_modificacion" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="estado" class="control-label">Estado</label>
						<div class="form-group">
							<input type="text" name="estado" value="<?php echo ($this->input->post('estado') ? $this->input->post('estado') : $negocio['estado']); ?>" class="form-control" id="estado" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="redsocial_facebook" class="control-label">Redsocial Facebook</label>
						<div class="form-group">
							<textarea name="redsocial_facebook" class="form-control" id="redsocial_facebook"><?php echo ($this->input->post('redsocial_facebook') ? $this->input->post('redsocial_facebook') : $negocio['redsocial_facebook']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="redsocial_twitter" class="control-label">Redsocial Twitter</label>
						<div class="form-group">
							<textarea name="redsocial_twitter" class="form-control" id="redsocial_twitter"><?php echo ($this->input->post('redsocial_twitter') ? $this->input->post('redsocial_twitter') : $negocio['redsocial_twitter']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="redsocial_instagram" class="control-label">Redsocial Instagram</label>
						<div class="form-group">
							<textarea name="redsocial_instagram" class="form-control" id="redsocial_instagram"><?php echo ($this->input->post('redsocial_instagram') ? $this->input->post('redsocial_instagram') : $negocio['redsocial_instagram']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="redsocial_youtube" class="control-label">Redsocial Youtube</label>
						<div class="form-group">
							<textarea name="redsocial_youtube" class="form-control" id="redsocial_youtube"><?php echo ($this->input->post('redsocial_youtube') ? $this->input->post('redsocial_youtube') : $negocio['redsocial_youtube']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="logo" class="control-label">Logo</label>
						<div class="form-group">
							<textarea name="logo" class="form-control" id="logo"><?php echo ($this->input->post('logo') ? $this->input->post('logo') : $negocio['logo']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="imagen_banner" class="control-label">Imagen Banner</label>
						<div class="form-group">
							<textarea name="imagen_banner" class="form-control" id="imagen_banner"><?php echo ($this->input->post('imagen_banner') ? $this->input->post('imagen_banner') : $negocio['imagen_banner']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="texto_banner" class="control-label">Texto Banner</label>
						<div class="form-group">
							<textarea name="texto_banner" class="form-control" id="texto_banner"><?php echo ($this->input->post('texto_banner') ? $this->input->post('texto_banner') : $negocio['texto_banner']); ?></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<label for="descripcion" class="control-label">Descripcion</label>
						<div class="form-group">
							<textarea name="descripcion" class="form-control" id="descripcion"><?php echo ($this->input->post('descripcion') ? $this->input->post('descripcion') : $negocio['descripcion']); ?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>