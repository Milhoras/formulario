<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Ubigeos Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('ubigeo/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Ubigeo</th>
						<th>Cod Ubigeo</th>
						<th>Nombre</th>
						<th>Estado</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($ubigeos as $u){ ?>
                    <tr>
						<td><?php echo $u['id_ubigeo']; ?></td>
						<td><?php echo $u['cod_ubigeo']; ?></td>
						<td><?php echo $u['nombre']; ?></td>
						<td><?php echo $u['estado']; ?></td>
						<td>
                            <a href="<?php echo site_url('ubigeo/edit/'.$u['id_ubigeo']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('ubigeo/remove/'.$u['id_ubigeo']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
