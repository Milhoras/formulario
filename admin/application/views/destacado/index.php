<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Destacados Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('destacado/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Destacado</th>
						<th>Titulo</th>
						<th>Id Negocio</th>
						<th>Descripcion</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($destacados as $d){ ?>
                    <tr>
						<td><?php echo $d['id_destacado']; ?></td>
						<td><?php echo $d['titulo']; ?></td>
						<td><?php echo $d['id_negocio']; ?></td>
						<td><?php echo $d['descripcion']; ?></td>
						<td>
                            <a href="<?php echo site_url('destacado/edit/'.$d['id_destacado']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('destacado/remove/'.$d['id_destacado']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
