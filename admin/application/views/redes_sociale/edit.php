<div class="row">
    <div class="col-md-12">
      	<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Redes Sociale Edit</h3>
            </div>
			<?php echo form_open('redes_sociale/edit/'.$redes_sociale['id_redsocial']); ?>
			<div class="box-body">
				<div class="row clearfix">
					<div class="col-md-6">
						<label for="id_negocio" class="control-label">Id Negocio</label>
						<div class="form-group">
							<input type="text" name="id_negocio" value="<?php echo ($this->input->post('id_negocio') ? $this->input->post('id_negocio') : $redes_sociale['id_negocio']); ?>" class="form-control" id="id_negocio" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="id_tipo" class="control-label">Id Tipo</label>
						<div class="form-group">
							<input type="text" name="id_tipo" value="<?php echo ($this->input->post('id_tipo') ? $this->input->post('id_tipo') : $redes_sociale['id_tipo']); ?>" class="form-control" id="id_tipo" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="nombre" class="control-label">Nombre</label>
						<div class="form-group">
							<input type="text" name="nombre" value="<?php echo ($this->input->post('nombre') ? $this->input->post('nombre') : $redes_sociale['nombre']); ?>" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="url" class="control-label">Url</label>
						<div class="form-group">
							<input type="text" name="url" value="<?php echo ($this->input->post('url') ? $this->input->post('url') : $redes_sociale['url']); ?>" class="form-control" id="url" />
						</div>
					</div>
					<div class="col-md-6">
						<label for="estado" class="control-label">Estado</label>
						<div class="form-group">
							<input type="text" name="estado" value="<?php echo ($this->input->post('estado') ? $this->input->post('estado') : $redes_sociale['estado']); ?>" class="form-control" id="estado" />
						</div>
					</div>
				</div>
			</div>
			<div class="box-footer">
            	<button type="submit" class="btn btn-success">
					<i class="fa fa-check"></i> Save
				</button>
	        </div>				
			<?php echo form_close(); ?>
		</div>
    </div>
</div>