<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Redes Sociales Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('redes_sociale/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Redsocial</th>
						<th>Id Negocio</th>
						<th>Id Tipo</th>
						<th>Nombre</th>
						<th>Url</th>
						<th>Estado</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($redes_sociales as $r){ ?>
                    <tr>
						<td><?php echo $r['id_redsocial']; ?></td>
						<td><?php echo $r['id_negocio']; ?></td>
						<td><?php echo $r['id_tipo']; ?></td>
						<td><?php echo $r['nombre']; ?></td>
						<td><?php echo $r['url']; ?></td>
						<td><?php echo $r['estado']; ?></td>
						<td>
                            <a href="<?php echo site_url('redes_sociale/edit/'.$r['id_redsocial']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('redes_sociale/remove/'.$r['id_redsocial']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
