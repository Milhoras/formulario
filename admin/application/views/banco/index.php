<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Bancos Listing</h3>
            	<div class="box-tools">
                    <a href="<?php echo site_url('banco/add'); ?>" class="btn btn-success btn-sm">Add</a> 
                </div>
            </div>
            <div class="box-body">
                <table class="table table-striped">
                    <tr>
						<th>Id Banco</th>
						<th>Nombre</th>
						<th>Estado</th>
						<th>Actions</th>
                    </tr>
                    <?php foreach($bancos as $b){ ?>
                    <tr>
						<td><?php echo $b['id_banco']; ?></td>
						<td><?php echo $b['nombre']; ?></td>
						<td><?php echo $b['estado']; ?></td>
						<td>
                            <a href="<?php echo site_url('banco/edit/'.$b['id_banco']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
                            <a href="<?php echo site_url('banco/remove/'.$b['id_banco']); ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span> Delete</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                                
            </div>
        </div>
    </div>
</div>
