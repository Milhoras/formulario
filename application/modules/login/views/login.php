<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="">
  <meta name="author" content="">


  <title>IziWEB</title>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets/lib/html5shiv/html5shiv.js"></script>
  <script src="/assets/lib/respond/respond.src.js"></script>
  <![endif]-->



  <!-- css -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


  <!-- js -->
  <!-- <script src="/assets/lib/modernizr/modernizr.js"></script> -->

  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

  <script src="<?php echo base_url();?>assets/js/library_validation.js"></script>
  <script src="<?php echo base_url();?>assets/js/validation.js"></script>
</head>






<body>

  <div class="sign-overlay"></div>

  <div class="signpanel"></div>



  <section id="core-wrapp">
    
    <div class="content-wrapp container">
  
      

        

        

      <?php if($this->session->flashdata('error') != ''): ?>

        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
        </div>

        <?php else: ?>
            <?php if($this->session->flashdata('success') != ''):?>
                <?php if($this->session->flashdata('success_tipo') == '2'):?>
                  <script type="text/javascript">
                    $(document).ready(function() {
                      $("#modal_confirmacion_cambio").modal('show');
                    });
                  </script>

                <?php else: ?>
                  <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success');?>
                  </div>
            <?php endif; ?>
        <?php endif; ?>

      <?php endif; ?>

        
      <div class="login-wrapp"> 

        <div class="row"> 

            <div class="login-head">
                <br><br>
                <p class="txt-welcome txt-gen">¡Bienvenido!</p>
                
                <div class="txt-title-sesion"><p class="in">iniciar sesión</p></div>
                
                <!-- <h4 class="panel-title">¡Bienvenido! Por favor identifícate.</h4> -->
              
            </div> <!-- fin login-head -->



            <div class="login-body">

              <form action="" method="post" onsubmit="return validar_login();" class="form-wrapp">

                <div class="col-xs-12 col-sm-7 form-line">
            
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Escribe tu RUC" name="ruc" id="ruc" required>
                  </div>
                </div> <!-- fin form-line -->
               
                
                <div class="col-xs-12 col-sm-7 form-line">
                  <button class="btn-gen btn-login">Ingresar</button>
                </div>

              </form>


              <hr class="invisible">

            </div> <!-- fin login-body -->

        </div> <!-- fin row -->


      </div> <!-- fin login-wrapp -->

      

    </div> <!-- fin content-wrapp -->

  </div><!-- fin core-wrapp -->



</body>
</html>



