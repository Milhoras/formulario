<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('validador/Validador_modelo', 'negocios/Negocios_modelo'));
	}

	/**
	 * Devuelve la página de inicio de sesión
	 * @access public
	 */
	public function index()
	{
		if($_POST)
		{
			$ruc = $this->input->post('ruc');
			$access = FALSE;
			
			$credential = $this->Validador_modelo->validar_usuario($ruc);
                   
			if($credential)
			{
				// conseguir sitios y roles del usuario
				$usuario = $this->Negocios_modelo->get_by_ruc($ruc);
			
				if(!empty($usuario))
				{
					$access = TRUE;
					if($access)
					{                                       
						// crear variables de sesión
				        $this->session->set_userdata('usuario', $usuario->nombre);
				        $this->session->set_userdata('nombre', $usuario->nombre);
				        $this->session->set_userdata('ruc', $usuario->ruc);
				        $this->session->set_userdata('id_negocio', $usuario->id_negocio);
				        redirect('negocios/'.$usuario->id_negocio);
					}
					else
					{
						$this->session->set_flashdata('error', 'No tiene permisos para este sitio');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Ha solicitado cambiar su contraseña. Por favor revise su correo.');
				}		
						

			}
			else
			{
				$this->session->set_flashdata('error', 'Usuario y/o Contraseña Incorrectos');
			}
		}

		$this->load->view('login');
	}

	



}