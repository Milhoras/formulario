<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Borra las sesiones iniciadas y devuelve la página de inicio de sesión
	 *
	 * @access public
	 */
	public function index()
	{
		$this->session->sess_destroy();
    	header('Location: '.base_url().'login');	
	}

}