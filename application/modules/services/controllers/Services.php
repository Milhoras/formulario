<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Services_modelo','votaciones/Votaciones_modelo', 'mecanicas/Mecanicas_modelo', 'matrices/Matrices_modelo'));	

	}

	
	public function index($id_mecanica)
	{	

		if (!empty($id_mecanica)){
			$data = array('registros' => $this->Services_modelo->get_json($id_mecanica));			
		}
		
		$this->load->view('service',$data);	
	}
}
 