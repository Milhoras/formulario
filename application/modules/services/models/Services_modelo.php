<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Services_modelo extends CI_Model
{	
	
	private $token = NULL;
	private $user = "backUser";
	private $pass = "EK26Deuo10";

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('string');
	}

	/*
     * @access private
     * @param int $id_mecanica id de la mecánica
     * @return Array Mecánicas - Opciones
    */
	function get_json($id_mecanica)
	{
		$data = array();
		$mecanica = $this->Mecanicas_modelo->generar_json($id_mecanica);

		$opciones = $this->Votaciones_modelo->generar_json_opciones($id_mecanica);
		$data['mecanica'] = $mecanica;
		$data['opciones'] = $opciones;

		return $data;
	}

	function connection()
	{
		
		$curl = curl_init(SERVICEPATH.'e2a58e9dba81a7869c8b2dc0e2915062');
	    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json','Content-Type: application/json'));
	    curl_setopt($curl, CURLOPT_USERPWD, $this->user.':'.$this->pass);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); 
	    curl_setopt($curl, CURLOPT_POST, TRUE); 
	    curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
	    $curl_response = curl_exec($curl);
	    $json = json_decode($curl_response);

	    if(!empty($json))
	    {
	      if($json->token)
	      {
	        $token = $json->token;    
	        return $token;
	      }
	    }
	    else
	    return FALSE;
   
	}


	
	

}
