<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller { // cambiar a MY_Controller cuando hayan usuarios

	public function __construct()
	{
		parent::__construct();
	}

	function show_head()
	{
		$this->load->view('head');
	}

	function show_sidebar()
	{	
		
		$this->load->view('sidebar');
	}

	function show_banner()
	{
		$this->load->view('banner');
	}


	function show_header()
	{
		$this->load->view('header');
	}

	function show_footer()
	{
		$this->load->view('footer');
	}


}