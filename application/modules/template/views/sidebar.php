<nav class="side-menu">
    
      <section>
        <header class="side-menu-title">Principales</header>
        <ul class="side-menu-list">
          
          <li class="green with-sub">
              <span>
                  <i class="font-icon font-icon-cogwheel"></i>
                  <span class="lbl">Administración</span>
              </span>
              <ul>
                  <li><a href="<?php echo base_url();?>sitios/administrar-sitios"><span class="lbl">Campañas</span></a></li>
              </ul>
          </li>
        </ul>
      </section>

  </nav><!--.side-menu-->