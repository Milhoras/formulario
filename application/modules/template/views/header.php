<div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Izipay</p>
        </div>
    </div>

    <div id="main-wrapper">
        <!-- ============================================================== -->
       <header class="topbar">
		<nav class="navbar top-navbar navbar-expand-md navbar-light">
			<!-- ============================================================== -->
			<!-- Logo -->
			<!-- ============================================================== -->
			<div class="navbar-header">
				<a class="navbar-brand" href="<?php echo base_url(); ?>">
					<!-- Logo icon -->
					<b>
						Izi
					</b>
					<!--End Logo icon -->
					<!-- Logo text -->
					<span>pay</span> 
				</a>

				
				<div class="menu-right">
					<a class="navbar-brand" href="<?php echo base_url();?>login/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Cerrar sesión</a>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- End Logo -->
			<!-- ============================================================== -->
		</nav>
	</header>