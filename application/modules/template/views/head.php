<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <title>IZIWEB</title>
  
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

  <!-- Bootstrap Core CSS -->
  <link href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom CSS -->
  <link href="<?php echo base_url();?>assets/css/style.css?v=2" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/colors/default-dark.css" id="theme" rel="stylesheet">

 