<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_nivel_acceso($nivel_acceso)
{
	switch (true) {
		case in_array($nivel_acceso, range(101, 150)):
			// solo puedo ver secciones mayores a 100 (invitado)
			$nivel = 100;
			break;
		case in_array($nivel_acceso, range(51, 100)):
			// solo puedo ver secciones mayores a 50 (redactor)
			$nivel = 50;
			break;
		case in_array($nivel_acceso, range(2, 50)):
			// solo puedo ver secciones mayores a 1 (moderador)
			$nivel = 1;
			break;
		case $nivel_acceso == "1":
			// es admin, conseguir todo
			$nivel = 0;
			break;
		default: 
			// por default tiene acceso de invitado (mayores a 100)
			$nivel = 100;
	}

	return $nivel;
}

function get_url_video($provider, $codigo)
{
	$url = '';
	switch ($provider) {
		case 'youtube':
			$url = 'https://youtu.be/'.$codigo;
			break;
		case 'vimeo':
			$url = 'https://vimeo.com/'.$codigo;
			break;
		case 'dailymotion':
			$url = 'http://dai.ly/'.$codigo;
			break;
		default:
			$url = $codigo;
			break;
	}

	return $url;
}

