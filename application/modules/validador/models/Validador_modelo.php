<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Validador_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'negocios';
	}

	function validar_usuario($ruc)
	{
		$this->db->from($this->__tabla);
		$this->db->where('ruc', $ruc);
		$user = $this->db->get();


		if($user->num_rows() > 0)
		{
			$usuario = $user->row();
			return $usuario;
		}

		return FALSE;
	}

	
	function validar_email($email)
	{
		if(!empty($email))
		{
			$this->db->from($this->__tabla);
			$this->db->where('email', $email);
			$user = $this->db->get();

			if($user->num_rows() > 0)
			{
				$usuario = $user->row();
				return $usuario;
			}	
		}	

		return FALSE;
	}

	function validar_token_email($token, $email)
	{
		$this->db->from($this->__tabla);
		$this->db->where('email', $email);
		$this->db->where('token', $token);
		$user = $this->db->get();


		if($user->num_rows() > 0)
		{
			$usuario = $user->row();
			return $usuario;
		}

		return FALSE;
	}

}