  <?php 
  $titulo = "Administración de campañas | RUBIK";

  echo Modules::run("template/show_head", $titulo);?>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/in-campania.min.css" />
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
</head>


<body>

  <header id="header">
    <div class="container">
      <div class="row">
          <h1 class="brand"><img src="<?php echo base_url();?>assets/img/logo.png"></h1>
          
      </div>
    </div>
  </header>
  
  <section id="core-wrapp">

    <div class="mobile-menu-left-overlay"></div>  

    <div class="content-wrapp container">

    <!-- MENSAJES -->
      <?php if($this->session->flashdata('error') != ''): ?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php else: ?>
        <?php if($this->session->flashdata('success') != ''):?>
          <div class="alert alert-success">
            <?php echo $this->session->flashdata('success');?>
          </div>
        <?php endif; ?>
      <?php endif; ?>
    <!-- MENSAJES -->



      <h2 class="txt-title">Administrador de campaña</h2>

      <div class="core-panel">

        <div class="row">
              
          <div class="top-bar">
            <div class="colum-wrapp colum-w1">
              <div class="colum">
                <p class="txt-colum">ID</p>
              </div>
            </div> <!-- fin colum -->

            <div class="colum-wrapp colum-w2">
              <div class="colum">
                <p class="txt-colum">Campaña</p>
              </div>
            </div> <!-- fin colum -->

            <div class="colum-wrapp colum-w3">
              <div class="colum">
                <p class="txt-colum">Radio</p>
              </div>
            </div> <!-- fin colum -->

            <div class="colum-wrapp colum-w4">
              <div class="colum">
                <p class="txt-colum">Fecha de Registro</p>
              </div>
            </div> <!-- fin colum -->

            <div class="colum-wrapp colum-w5">
              <div class="colum">
                <p class="txt-colum">Estado</p>
              </div>
            </div> <!-- fin colum -->

            <div class="colum-wrapp colum-w6">
              <div class="colum">
                <p class="txt-colum">Año</p>
              </div>
            </div> <!-- fin colum -->

            <div class="colum-wrapp colum-w7">
              <div class="colum reset">
                <p class="txt-colum">Acciones</p>
              </div>
            </div> <!-- fin colum -->

          </div> <!-- fin top-bar -->


          <div id="card">
          
              <form name='materia'>
               
                <?php if(!empty($campanias)):?>
                  <?php foreach($campanias as $campania):?>

                  <div id="<?php echo $campania->id_campania;?>" class="line">

                    <div class="colum-wrapp colum-w1">
                      <div class="colum">
                        <p class="txt-gen txt-dato"><?php echo $campania->id_campania;?></p>
                      </div>
                    </div>

                    <div class="colum-wrapp colum-w2">
                      <div class="colum">
                        <p class="txt-gen txt-dato"><?php echo htmlspecialchars($campania->nombre);?></p>
                      </div>
                    </div>

                    <div class="colum-wrapp colum-w3">
                      <div class="colum">
                        <p class="txt-gen txt-dato"><?php echo $campania->radio;?></p>
                      </div>
                    </div>


                    <div class="colum-wrapp colum-w4">
                      <div class="colum">
                        <p class="txt-gen txt-dato"><?php echo $campania->fecha_registro; ?></p>
                      </div>
                    </div>


                    <div class="colum-wrapp colum-w5">
                      <div class="colum">
                        <p class="txt-gen txt-dato">
                        <?php 
                          switch ($campania->estado) {
                             case '1':
                               echo "Activo";
                               break;

                            case '2':
                               echo "Cerrado";
                               break;
                             
                             default:
                               echo "Borrado";
                               break;
                           } ?></p>
                      </div>
                    </div>

                    <div class="colum-wrapp colum-w6">
                      <div class="colum">
                        <p class="txt-gen txt-dato"><?php echo $campania->anio; ?></p>
                      </div>
                    </div>


                    <div class="colum-wrapp colum-w7">

                      <div class="colum reset-border-r">

                        <div class="center-gen">

                          <div class="hide-mob">

                            <a href="<?php echo base_url();?>mecanicas/<?php echo $campania->id_campania;?>" alt="" class="btn-gen btn-action">
                              <span class="tooltip-gen tooltip-gen-1 animated fadeIn">Ver mecánicas</span>
                              <i class="fas fa-eye"></i>
                            </a>

                            <a href="<?php echo base_url();?>campanias/formulario_edicion_campania/<?php echo $campania->id_campania;?>" class="btn-gen btn-action" alt="">
                              <span class="tooltip-gen tooltip-gen-1 animated fadeIn">Editar campaña</span>
                              <i class="fas fa-pen"></i>
                            </a>
                       
                            <a href="javascript:void(0);" class="btn-gen btn-action eliminar" alt="" id="<?php echo $campania->id_campania; ?>">
                              <span class="tooltip-gen tooltip-gen-1 animated fadeIn">Eliminar campaña</span>
                              <i class="fas fa-trash"></i>
                            </a>
                            
                          </div>

                          <div class="show-mob">
                            <div href="" class="btn-gen btn-action show-mob-action">
                              <i class="fas fa-ellipsis-h"></i>
                            </div>
                          </div>

                        </div> <!-- fin center-gen -->
                        
                      </div>

                    </div>   

                  </div> <!-- fin line -->


                  <?php endforeach; ?>
                <?php endif; ?>
              

              </form>

          </div> <!-- fin card -->

        </div> <!-- fin row -->

        <?php echo $pagination; ?>

      </div><!--.core-panel-->


     <!--  <nav class="nav">
        <ul class="nav-ul">
          <li class="nav-arrow nav-prev nav-block"><i class="fas fa-chevron-left"></i></li>

          <li class="nav-num nav-select">1</li>

          <li class="nav-num">2</li>

          <li class="nav-num">3</li>

          <li class="nav-arrow nav-next"><i class="fas fa-chevron-right"></i></li>
        </ul>
      </nav> -->
    
    </div> <!-- fin content-wrapp -->

  </section> <!-- fin core-wrapp -->




  <?php echo Modules::run('template/show_footer');?>

  <div class="wrapp-create">
    <div class="container">
      <div class="row">
        <a href="<?php echo base_url();?>campanias/formulario_registro_campania" class="btn-gen btn-create">
          <span class="tooltip-gen tooltip-gen-3 animated fadeIn">Crear campaña</span>
          <i class="fas fa-plus"></i>
        </a>
      </div>
    </div>
  </div>



  <!-- Modal -->
  <div class="modal fade" id="modal_eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
          
        </div> <!-- fin modal-header -->

        <div class="modal-body">
          <p class="txt-gen modal-title">ELIMINAR CAMPAÑA</p>

          <p class="txt-gen modal-txt">¿Seguro que deseas cerrar esta campaña?</p>

          <div class="center-gen">
            <button type="button" class="btn-none" id="modal-btn-si-eliminar">eliminar</button>
            <button type="button" class="btn-gen" id="modal-btn-no-eliminar" data-dismiss="modal" aria-label="Close">no eliminar</button>
          </div>
        </div> <!-- fin modal-body -->
        
      </div>

    </div>

  </div>
  


<script>
$(document).ready(function() {

var id = 0;
  // Eliminar
  var modalConfirmEliminar = function(callback){
  
    $(".eliminar").on("click", function(){
      id = $(this).attr('id');
      $("#modal_eliminar").modal('show');
    });

    $("#modal-btn-si-eliminar").on("click", function(){
      callback(true, id);
      $("#modal_eliminar").modal('hide');
    });
    
    $("#modal-btn-no-eliminar").on("click", function(){
      callback(false);
      $("#modal_eliminar").modal('hide');
    });
  };

  modalConfirmEliminar(function(confirm, id){
    if(confirm){
      
      //Acciones si el usuario confirma
      console.log(id);
      window.location.href = '<?php echo base_url();?>campanias/eliminar_campania/'+id;
    }else{
      //Acciones si el usuario no confirma
      $("#modal_eliminar").modal('hide');
    }
  });
  // Eliminar


  // $(".show-mob").click(function(){
  //     $(this).siblings('hide-mob').toggle();
  // });



$(document)
    .ready(function () {
    $('.show-mob').click(function () {
        $(this).siblings('.hide-mob').toggle();
    });
});

  $(function () {

    $(window).bind("resize", function () {
        // console.log($(this).width())
        if ($(this).width() < 550) {
            $('.hide-mob').addClass('hide-mob-resize')
            $('.hide-mob').css("display", "none")
        } else {
            $('.hide-mob').removeClass('hide-mob-resize')
            $('.hide-mob').css("display", "block")
        }
    }).trigger('resize');
  })

  

});
</script>

</body>
</html>
