  <?php 
  echo Modules::run("template/show_head");?>
</head>

<body class="fix-header fix-sidebar card-no-border">

    <?php echo Modules::run("template/show_header");?> 
    
    <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="wrapper" style="background: #f4f6f9;padding-top: 70px;">
            <div class="container">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <div class="row page-titles">
                        <div class="col-md-5 align-self-center">
                            <h3 class="text-themecolor">MI IZIWEB</h3>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body p-b-0">
                            <h4 class="card-title">Crea tú página web</h4>
                            <h6 class="card-subtitle">completa los 5 pasos básicos</h6> <strong><span class="text-danger">Para la grabación correcta de la información debera
                            ingresar los datos que estan marcados con un (*) de color rojo. <br>La data solo se grabará cuando al final del Paso 5 haga click en el boton de <br>
                            <span class="text-danger">"Guardar Web" </span> </strong> </span></div>

                            
                           

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#paso1" role="tab" aria-expanded="true">
                                    <span class="hidden-sm-up"><i class="ti-home"></i></span>
                                    <span class="hidden-xs-down">Paso 1</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#paso2" role="tab" aria-expanded="false">
                                    <span class="hidden-sm-up"><i class="ti-user"></i></span> 
                                    <span class="hidden-xs-down">Paso 2</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#paso3" role="tab" aria-expanded="false">
                                    <span class="hidden-sm-up"><i class="ti-email"></i></span>
                                    <span class="hidden-xs-down">Paso 3</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#paso4" role="tab" aria-expanded="false">
                                    <span class="hidden-sm-up"><i class="ti-email"></i></span>
                                    <span class="hidden-xs-down">Paso 4</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#paso5" role="tab" aria-expanded="false">
                                    <span class="hidden-sm-up"><i class="ti-email"></i></span>
                                    <span class="hidden-xs-down">Paso 5</span>
                                </a>
                            </li>
                        </ul>

                        <?php if($this->session->flashdata('error') != ''): ?>
                            <div class="alert alert-danger">
                                <?php echo $this->session->flashdata('error'); ?>
                            </div>
                        <?php else: ?>

                        <form id="basicForm" action="<?php echo base_url();?>negocios/actualizar_negocio/<?php echo $id_negocio; ?>" method="post" onsubmit="return validar_registro()" class="form-gen" enctype="multipart/form-data">
                        <input type="hidden" id="productosNegocio" name="productosNegocio">
                        <input type="hidden" id="enviosNegocio" name="enviosNegocio">
                        

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <!--tab1-->
                            <div class="tab-pane active" id="paso1" role="tabpanel" aria-expanded="true">
                                <div class="p-20">
                                    <form novalidate>
                                        <div class="form-body">
                                            <h2><strong>Datos del negocio</strong></h2>
                                            <div class="row p-t-20">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Nombre del negocio <span class="text-danger">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="nombre" name="nombre" value="<?php echo $negocio->nombre != '' ? $negocio->nombre : "";?>" required class="form-control" placeholder="Nombre del negocio" data-validation-required-message="Este campo es requerido">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Teléfono del negocio <span class="text-danger">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="telefono" name="telefono" value="<?php echo $negocio->telefono != '' ? $negocio->telefono : "";?>" class="form-control" required data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="No Characters Allowed, Only Numbers" placeholder="+51...">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">Correo electrónico <span class="text-danger">*</span></label>
                                                        <div class="controls">
                                                            <input type="email" id="email" name="email" value="<?php echo $negocio->email != '' ? $negocio->email : "";?>" data-validation-regex-regex="([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})" data-validation-regex-message="Enter Valid Email" class="form-control form-control-danger" placeholder="micorreo@correo...">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <h3 class="box-title m-t-40">Local</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <h5>¿Cuentas con local? <span class="text-danger">*</span></h5>
                                                        <fieldset class="controls">
                                                            <label class="custom-control custom-radio">
                                                                <input type="radio" value="1" name="local" id="local_si" class="custom-control-input" <?php echo $negocio->local == '1' ? 'checked' : "";?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">Sí</span> </label>
                                                        </fieldset>
                                                        <fieldset>
                                                            <label class="custom-control custom-radio">
                                                                <input type="radio" value="2" name="local" id="local_no" class="custom-control-input" <?php echo $negocio->local == '2' ? 'checked' : "";?>> <span class="custom-control-indicator"></span> <span class="custom-control-description">No</span> </label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Dirección</label>
                                                        <div class="controls">
                                                            <input type="text" id="direccion" name="direccion" value="<?php echo $negocio->direccion != '' ? $negocio->direccion : "";?>" class="form-control" placeholder="Dirección">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Departamento</label>
                                                        <div class="controls">

                                                        <select name="id_departamento" id="id_departamento" class="form-control" required onChange="javascript:change_provincia('id_departamento','id_provincia')">
                                                        <option value="0">Selecciona tu departamento</option>
                                                        <?php foreach ($departamentos as $departamento):  ?>
                                                       
                                                            <?php 
                                                            if(!empty($departamentos)):  
                                                                    if ($departamento->cod_ubigeo == $negocio->id_departamento) : ?>
                                                                        <option value="<?php echo $departamento->cod_ubigeo; ?>" selected><?php echo $departamento->nombre; ?></option>                         
                                                                <?php else: ?>
                                                                        <option value="<?php echo $departamento->cod_ubigeo; ?>"><?php echo $departamento->nombre; ?></option>   
                                                                <?php endif;?>
                                                            <?php endif; ?>
                                                         <?php endforeach; ?> 
                                                         </select>                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Provincia</label>
                                                        <div class="controls">
                                                            <select name="id_provincia" id="id_provincia" class="form-control"  onChange="javascript:change_distrito('id_provincia','id_distrito')">
                                                                <option value="0">Selecciona tu provincia</option>

                                                                <?php if(!empty($provincias)): ?>
                                                                    <?php foreach ($provincias as $provincia):  ?>
                                                       
                                                                    <?php 
                                                                            if ($provincia->cod_ubigeo == $negocio->id_provincia) : ?>
                                                                                <option value="<?php echo $provincia->cod_ubigeo; ?>" selected><?php echo $provincia->nombre; ?></option>                         
                                                                        <?php else: ?>
                                                                                <option value="<?php echo $provincia->cod_ubigeo; ?>"><?php echo $provincia->nombre; ?></option>   
                                                                        <?php endif;?>
                                                                    <?php endforeach; ?> 

                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Distrito</label>
                                                        <div class="controls">
                                                            <select name="id_distrito" id="id_distrito" class="form-control">
                                                                <option value="0">Selecciona tu distrito</option>

                                                                <?php if(!empty($distritos)): ?>
                                                                    <?php foreach ($distritos as $distrito):  ?>
                                                       
                                                                    <?php 
                                                                            if ($distrito->cod_ubigeo == $negocio->id_distrito) : ?>
                                                                                <option value="<?php echo $distrito->cod_ubigeo; ?>" selected><?php echo $distrito->nombre; ?></option>                         
                                                                        <?php else: ?>
                                                                                <option value="<?php echo $distrito->cod_ubigeo; ?>"><?php echo $distrito->nombre; ?></option>   
                                                                        <?php endif;?>
                                                                    <?php endforeach; ?> 

                                                                <?php endif; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--/span-->
                                                


                                            </div>
                                         <!--   <h3 class="box-title m-t-40">Cuenta de abono</h3>-->
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <!--<label class="control-label">Número de cuenta<span class="text-danger">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="numero_cuenta" name="numero_cuenta" value="<?php echo $negocio->numero_cuenta != '' ? $negocio->numero_cuenta : "";?>" class="form-control" placeholder="Número de cuenta" required data-validation-containsnumber-regex="(\d)+" required data-validation-required-message="Ingresa tu número de cuenta" data-validation-containsnumber-message="No está permitido los caracteres">
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <!--<label class="control-label">CCI<span class="text-danger">*</span></label>
                                                        <div class="controls">
                                                            <input type="number" id="numero_cci" name="numero_cci" value="<?php echo $negocio->numero_cci != '' ? $negocio->numero_cci : "";?>" class="form-control form-control-danger" placeholder="cci..." required data-validation-containsnumber-regex="(\d)+" required data-validation-required-message="Ingresa tu CCI" data-validation-containsnumber-message="No está permitido los caracteres">
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <!--<label class="control-label">Tu banco<span class="text-danger">*</span></label>
                                                        <div class="controls">
                                                            <select name="id_banco" id="id_banco" class="form-control" required data-validation-required-message="Selecciona un banco">
                                                                <option value="0">Selecciona tu banco</option>
                                                                <?php foreach ($bancos as $banco):  ?>
                                                       
                                                                <?php 
                                                                if(!empty($bancos)):  
                                                                        if ($banco->id_banco == $negocio->id_banco) : ?>
                                                                            <option value="<?php echo $banco->id_banco; ?>" selected><?php echo $banco->nombre; ?></option>                         
                                                                    <?php else: ?>
                                                                            <option value="<?php echo $banco->id_banco; ?>" selected><?php echo $banco->nombre; ?></option>   
                                                                    <?php endif;?>
                                                                <?php endif; ?>
                                                                <?php endforeach; ?>   
                                                            </select>                                                            
                                                        </div>-->
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <h3 class="box-title m-t-40">Redes sociales</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Facebook</label>
                                                        <div class="controls">
                                                            <input type="text" id="redsocial_facebook" name="redsocial_facebook" class="form-control" placeholder="url Facebook" value="<?php echo $negocio->redsocial_facebook != '' ? $negocio->redsocial_facebook : "";?>">
                                                        </div>
                                                        <small class="form-control-feedback">Ingresa la url de tu Facebook</small> </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Twitter</label>
                                                        <div class="controls">
                                                            <input type="text" id="redsocial_twitter" name="redsocial_twitter" class="form-control" placeholder="url Twitter" value="<?php echo $negocio->redsocial_twitter != '' ? $negocio->redsocial_twitter : "";?>">
                                                        </div>
                                                        <small class="form-control-feedback">Ingresa la url de tu Twitter</small> </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Instagram</label>
                                                        <div class="controls">
                                                            <input type="text" id="redsocial_instagram" name="redsocial_instagram" class="form-control" placeholder="url Instagram" value="<?php echo $negocio->redsocial_instagram != '' ? $negocio->redsocial_instagram : "";?>" >
                                                        </div>
                                                        <small class="form-control-feedback">Ingresa la url de tu Instagram</small> </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">Youtube</label>
                                                        <div class="controls">
                                                            <input type="text" id="redsocial_youtube" name="redsocial_youtube" class="form-control" placeholder="url Youtube"  value="<?php echo $negocio->redsocial_youtube != '' ? $negocio->redsocial_youtube : "";?>">
                                                        </div>
                                                        <small class="form-control-feedback">Ingresa la url de tu Youtube</small> </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>
                                        <div class="form-actions text-right"><strong>Todavía los datos ingresados no han sido guardados, continue con el llenado</strong>
                                            <a data-toggle="tab" href="#paso2" role="tab" class="btn btn-info btnNext">Siguiente <i class="ti-arrow-right"></i></a>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                            <!--tab2-->
                            <div class="tab-pane" id="paso2" role="tabpanel" aria-expanded="false">
                                <div class="p-20">
                                    <div class="form-body">
                                        <h2><strong>Contenido Web</strong></h2>
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group" id="content_imagen_logo">
                                                    <h5>Logotipo <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                    <?php if(!empty($negocio->logo)) :?>
                                                        <div class="wrapp-up">
                                                            <img src="<?php echo base_url()."/images/negocios/".$negocio->logo; ?>" width="300" id="logo_negocio" class="img-edit">
                                                            <a href="#" class="eliminar_logo btn-gen btn-circle btn-trush"><i class="fas fa-trash"></i></a>
                                                        </div>

                                                    <?php else: ?>
                                                        <input type="file" name="logo" id="logo" class="form-control" required="" accept="image/x-png"> <div class="help-block"></div>
                                                        <small class="form-control-feedback"> Solo imágenes PNG* </small>
                                                    <?php endif; ?>  
                                                    </div>
                                                       
                                                </div>
                                            </div>
                                        </div>
                                        <h5>Banner Inicio</h5>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group" id="content_imagen_banner">
                                                    <h5>Imagen del banner <span class="text-danger">*</span></h5>
                                                    <div class="controls">
                                                    <?php if(!empty($negocio->imagen_banner)) :?>
                                                        <div class="wrapp-up">
                                                            <img src="<?php echo base_url()."/images/negocios/".$negocio->imagen_banner; ?>" width="200" id="banner_negocio" class="img-edit">
                                                            <a href="#" class="eliminar_banner btn-gen btn-circle btn-trush"><i class="fas fa-trash"></i></a>
                                                        </div>

                                                    <?php else: ?>
                                                        <input type="file" name="imagen_banner"  id="imagen_banner" class="form-control" required="" accept="image/jpeg"> <div class="help-block"></div>
                                                        <small class="form-control-feedback">Formato JPG, orientación horizontal, tamaño mínimo 1800px de ancho* </small>
                                                    <?php endif; ?>   
                                                    </div>     

                                                        
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Texto Banner</label>
                                                    <input type="text" name="texto_banner" id="texto_banner" class="form-control" placeholder="Texto del banner" value="<?php echo $negocio->texto_banner != '' ? $negocio->texto_banner : "";?>">
                                                    <small class="form-control-feedback">Este texto acompañará al banner</small> </div>
                                            </div>
                                        </div>
                                        <h5>¿Quiénes somos?</h5>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea name="descripcion" id="descripcion" class="form-control" rows="5" ><?php echo $negocio->descripcion != '' ? $negocio->descripcion : "";?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="box-title m-t-40">3 Destacados</h3>
                                        <h5>Tres beneficios, fortalezas o atributos de tu marca que quieras destacar.</h5>
                                        <hr>
                                       
                                       <?php if(!empty($destacados)):
                                                $x = 1;
                                                foreach($destacados as $destacado): ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" maxlength="25" name="titulo_<?php echo $x; ?>" id="titulo_<?php echo $x; ?>" class="form-control" value="<?php echo $destacado->titulo != '' ? $destacado->titulo : "";?>">
                                                    <small class="form-control-feedback">Máximo 25 caracteres</small>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" maxlength="150" name="texto_<?php echo $x; ?>" id="texto_<?php echo $x; ?>" class="form-control" value="<?php echo $destacado->descripcion != '' ? $destacado->descripcion : "";?>">
                                                    <small class="form-control-feedback">Máximo 150 caracteres</small>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                            $x++;    
                                    endforeach;
                                            else: ?>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" maxlength="25" name="titulo_1" id="titulo_1" class="form-control" value="" tooltip="Titulo 1">
                                                    <small class="form-control-feedback">Máximo 25 caracteres</small>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" maxlength="150" name="texto_1" id="texto_1" class="form-control" value="Texto 1">
                                                    <small class="form-control-feedback">Máximo 150 caracteres</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" maxlength="25" name="titulo_2" id="titulo_2" class="form-control" value="Título 2">
                                                    <small class="form-control-feedback">Máximo 25 caracteres</small>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" maxlength="25" name="texto_2" id="texto_2" class="form-control" value="Texto 2">
                                                    <small class="form-control-feedback">Máximo 150 caracteres</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" maxlength="25" name="titulo_3" id="titulo_3" class="form-control" value="Título 3">
                                                    <small class="form-control-feedback">Máximo 25 caracteres</small>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" maxlength="25" name="texto_3" id="texto_3" class="form-control" value="Texto 3">
                                                    <small class="form-control-feedback">Máximo 150 caracteres</small>
                                                </div>
                                            </div>
                                        </div>

                                        <?php endif; ?>

                                        <div class="card card-inverse" style="background-color: #333; border-color: #333;">
                                            
                                            <div class="card-body">
                                                <h3 class="card-title">Ejemplo:</h3>
                                                <div class="row" style="color: rgba(255, 255, 255, 0.65);">
                                                    <div class="col-md-4">
                                                        <h4 style="color: rgba(255, 255, 255, 0.65);">Título:</h4>
                                                        <p class="card-text">Los mejores insumos</p>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <h4 style="color: rgba(255, 255, 255, 0.65);">Texto:</h4>
                                                        <p class="card-text">Escogemos minuciosamente los mejores ingredientes y tenemos la receta perfecta para brindarles
                                                            brownies y blondies que se volverán tus favoritos.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="box-title m-t-40">Elije tu plantilla</h3>
                                        <h5>En el siguiente enlace vas a poder descargar 9 plantillas. Todas se pueden aplicar a tu rubro, los temas e imágenes son de referencia.</h5>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <a href="https://broom.pe/plantillas-broom.zip"><button type="button" class="btn waves-effect waves-light btn-block btn-info">Ver plantillas</button></a>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label text-right">Selecciona una plantilla:</label>
                                                    <select name="id_plantilla" id="id_plantilla" class="form-control" required data-validation-required-message="Selecciona una plantilla">
                                                                <option value="0"></option>
                                                                <?php foreach ($plantillas as $plantilla):  ?>
                                                       
                                                                <?php 
                                                                if(!empty($plantillas)):  
                                                                        if ($plantilla->id_plantilla == $negocio->id_plantilla) : ?>
                                                                            <option value="<?php echo $plantilla->id_plantilla; ?>" selected><?php echo $plantilla->nombre; ?></option>                         
                                                                    <?php else: ?>
                                                                            <option value="<?php echo $plantilla->id_plantilla; ?>"><?php echo $plantilla->nombre; ?></option>   
                                                                    <?php endif;?>
                                                                <?php endif; ?>
                                                                <?php endforeach; ?>   
                                                            </select>   
                                                </div>
                                            </div>
                                            <div class="col-md-8 text-center">
                                                <img class="card-img-top img-responsive" id="imagen_plantilla" src="<?php echo $negocio->id_plantilla != '0' ? base_url()."images/plantillas/".$negocio->id_plantilla.".jpg" : "";?>" alt="Card image cap">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions text-right p-t-20"><strong>Todavía los datos ingresados no han sido guardados, continue con el llenado</strong>
                                        <a data-toggle="tab" href="#paso1" role="tab" class="btn btn-info btnPrevious"><i class="ti-arrow-left"></i> Anterior </a>
                                        <a data-toggle="tab" href="#paso3" role="tab" class="btn btn-info btnNext">Siguiente <i class="ti-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!--tab3-->
                            <div class="tab-pane" id="paso3" role="tabpanel" aria-expanded="false">
                                <div class="p-20">
                                    <div class="list-products" id="listaProducto">
                                        <table class="table vm no-th-brd pro-of-month">
                                            <thead>
                                                <tr>
                                                    <th colspan="3">Imagen y nombre</th>
                                                    <th>Precio</th>
                                                    <th width="100">Acción</th>
                                                </tr>
                                            </thead>
                                            <tbody id="sortable-paso3">
                                            <?php if(!empty($productos)) :
                                                    foreach($productos as $producto) :?>
                                                        <tr>
                                                            <td width="50"><i class="ti-arrows-vertical"></i></td>
                                                            <td style="width:50px;"><span class="round"><img src="<?php echo base_url();?>images/productos/<?php echo $producto->imagen; ?>" alt="user" width="50"></span></td>
                                                            <td><?php echo $producto->nombre; ?></td>
                                                            <td>S/ <?php echo $producto->precio; ?></td>
                                                            <td>
                                                                <!--<button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i> </button>-->
                                                            </td>
                                                        </tr>

                                            <?php   endforeach; 
                                                endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-body">
                                        <h2><strong>Productos</strong></h2>
                                        <div class="row p-t-20 detalleProducto">
                                            <div class="col-md-8">
                                                <div class="form-group nombreProducto">
                                                    <label class="control-label">Nombre Producto</label>
                                                    <input type="text" id="nombre_producto" name="nombre_producto[]" class="form-control" placeholder="Nombre del producto">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group imagenProducto">
                                                    <h5>Imagen del producto</h5>
                                                    <div class="controls">
                                                        <input type="file" onchange="readURL(this);" name="imagen_producto[]" id="imagen_producto" class="form-control"> <div class="help-block"></div></div>
                                                        <small class="form-control-feedback">Formato JPG / Tamaño mínimo 1000px x 1000px</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group codigoProducto">
                                                    <label class="control-label">Código del producto</label>
                                                    <input type="text" id="codigo_producto" name="codigo_producto[]" class="form-control" placeholder="Código del producto">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group categoriaProducto">
                                                    <label class="control-label">Categoría</label>
                                                    <input type="text" id="categoria_producto" name="categoria_producto[]" class="form-control" placeholder="Categoría del producto">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group precioProducto">
                                                    <label class="control-label">Precio</label>
                                                    <input type="number" id="precio_producto" name="precio_producto[]" class="form-control" placeholder="Precio del producto" min="0" max="10" step="0.10" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group descripcionProducto">
                                                    <label class="control-label">Descripción del producto</label>
                                                    <textarea id="descripcion_producto" name="descripcion_producto[]" class="form-control" rows="5" maxlength="250"></textarea>
                                                    <small class="form-control-feedback" id="charNumProducto"> Máximo 250 caracteres </small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <button type="button" class="btn waves-effect waves-light btn-info" id="addProducto"><i class="ti-save"></i> Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    
                                    <div class="form-actions text-right">
                                        <a data-toggle="tab" href="#paso2" role="tab" class="btn btn-info btnPrevious"><i class="ti-arrow-left"></i> Anterior </a>
                                        <a data-toggle="tab" href="#paso4" role="tab" class="btn btn-info btnNext">Siguiente <i class="ti-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!--tab4-->
                            <div class="tab-pane" id="paso4" role="tabpanel" aria-expanded="false">
                                <div class="p-20">
                                    <h2><strong>Zonas de envío</strong></h2>
                                    <h4>¿A qué distritos llega tu delivery?</h4>
                                    <div class="list-products table-responsive" id="listaEnvio">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th  colspan="2">Departamento</th>
                                                    <th>Distrito</th>
                                                    <th width="100">Acción</th>
                                                </tr>
                                                <?php if(!empty($envios)):
                                                        foreach($envios as $envio) :?>
                                                            <tr>
                                                                <td width="30" class="envio_id" data-envio-id='<?php echo $envio->id_envio; ?>'><i class="ti-arrows-vertical"></i></td>
                                                                <td style="width:50px;"><?php echo $envio->departamento;?></td>
                                                                <td><?php echo $envio->distrito;?></td>
                                                                <td>
                                                                    <!--<button type="button" class="btn btn-warning btn-circle" id="deleteEnvio<?php //echo $envio->id_envio;?>"><i class="fa fa-times"></i> </button>-->
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; 
                                                        endif; ?>
                                            </thead>
                                            <tbody id="sortable-paso4">                                               
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-body">
                                        <h2><strong>Envíos</strong></h2>
                                        <div class="row p-t-20" id="detalleEnvio">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Departamento</label>
                                                    <select name="departamento_envio" id="departamento_envio" class="form-control" onChange="javascript:change_provincia('departamento_envio','provincia_envio')">
                                                        <option value="0">Selecciona tu departamento</option>
                                                        <?php if(!empty($departamentos)):  ?> 
                                                            <?php foreach ($departamentos as $departamento):  ?>                                                                           
                                                                <option value="<?php echo $departamento->cod_ubigeo; ?>"><?php echo $departamento->nombre; ?></option>   
                                                            <?php endforeach; ?> 
                                                        <?php endif; ?>                                                            
                                                            </select>     

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Provincia</label>
                                                    <select name="provincia_envio" id="provincia_envio" class="form-control" onChange="javascript:change_distrito('provincia_envio','distrito_envio')">
                                                        <option value="0">Selecciona tu provincia</option>                                                        
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">Distrito</label>
                                                    <select name="distrito_envio" id="distrito_envio" class="form-control">
                                                        <option value="0">Selecciona tu distrito</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Costo de envío</label>
                                                    <input type="number" id="costo_envio" name="costo_envio" class="form-control" placeholder="Costo de envío">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Tiempo de entrega</label>
                                                    <input type="text" id="tiempo_envio" name="tiempo_envio" class="form-control" placeholder="Tiempo de entrega">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <button type="button" class="btn waves-effect waves-light btn-info" id="addEnvio"><i class="ti-save"></i> Guardar</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="form-actions text-right">
                                        <a tdata-toggle="tab" href="#paso3" role="tab" class="btn btn-info btnPrevious"><i class="ti-arrow-left"></i> Anterior </a>
                                        <a tdata-toggle="tab" href="#paso5" role="tab" class="btn btn-info btnNext">Siguiente <i class="ti-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <!--tab5-->
                            <div class="tab-pane " id="paso5" role="tabpanel" aria-expanded="false">
                                <div class="p-20">
                                    <h2><strong>Dominio</strong></h2>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">¿Tienes dominio?</label>
                                                <div class="form-check">
                                                    <label class="custom-control custom-radio">
                                                        <input id="dominio_si" name="dominio" type="radio" class="custom-control-input" value="1" <?php echo (!empty($dominio)) ? ($dominio->dominio != '' ? 'checked' : "") : '';?>>
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Sí</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input id="dominio_no" name="dominio" type="radio" class="custom-control-input" value="2"<?php echo (!empty($dominio)) ? ($dominio->dominio != '' ? 'checked' : "") : '';?>>
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">No</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">dominio</label>
                                                <input type="text" id="dominio" name="nombre_dominio" class="form-control" placeholder="www.dominio" value="<?php echo (!empty($dominio)) ? ($dominio->nombre != '' ? $dominio->nombre : "") : '';?>">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">¿Selecciona una opción?</label>
                                                <div class="form-check">
                                                    <label class="custom-control custom-radio">
                                                        <input id="tld_dominio_pe" name="tld_dominio" type="radio" <?php echo (!empty($dominio)) ? ($dominio->tld == 'pe' ? "checked" : "") : '';?> class="custom-control-input" value="pe">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">.PE</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input id="tld_dominio_compe" name="tld_dominio" type="radio" class="custom-control-input" <?php echo (!empty($dominio)) ? ($dominio->tld == 'com.pe' ? "checked" : "") : '';?> value="com.pe">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">.COM.PE</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <small class="form-control-feedback"> De no encontrar disponibilidad del dominio elegido nos contactaremos para sugerirles opciones  </small> 
                                        </div>
                                    </div>
                                    <h3 class="box-title m-t-40">Datos para el registro de dominio</h3>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Número</label>
                                                <input type="number" id="numero_dominio" name="numero_dominio" class="form-control" placeholder="número" value="<?php echo (!empty($dominio)) ? ($dominio->numero != '' ? $dominio->numero : "") : '';?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">¿Selecciona una opción?</label>
                                                <div class="form-check">
                                                    <label class="custom-control custom-radio">
                                                        <input id="dominio_3" name="ruc_dni" type="radio" <?php echo (!empty($dominio)) ? ($dominio->ruc_dni != '' ? "checked" : "") : '';?> class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">RUC</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input id="dominio_4" name="ruc_dni" type="radio" class="custom-control-input" <?php echo (!empty($dominio)) ? ($dominio->ruc_dni != '' ? "checked" : "") : '';?>>
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">DNI</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Razón social / Nombre</label>
                                                <input type="text" id="razon_social" name="razon_social" class="form-control" placeholder="Razón social / Nombre" value="<?php echo (!empty($dominio)) ? ($dominio->razon_social != '' ? $dominio->razon_social : "") : '';?>">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">¿Selecciona una opción?</label>
                                                <div class="form-check">
                                                    <label class="custom-control custom-radio">
                                                        <input id="tipo_razon_nombre" name="tipo_razon_nombre" type="radio" value="1" <?php echo (!empty($dominio)) ? ($dominio->tipo_razon_nombre != '' ? "checked" : "") : '';?> class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Razón social</span>
                                                    </label>
                                                    <label class="custom-control custom-radio">
                                                        <input id="tipo_razon_nombre" name="tipo_razon_nombre" type="radio"  value="2" <?php echo (!empty($dominio)) ? ($dominio->tipo_razon_nombre != '' ? "checked" : "") : '';?> class="custom-control-input">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description">Nombre</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input type="text" id="email_dominio" name="email_dominio" class="form-control" placeholder="Email" value="<?php echo (!empty($dominio)) ? ($dominio->email != '' ? $dominio->email : "") : '';?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Teléfono</label>
                                                <input type="text" id="telefono_dominio" name="telefono_dominio" class="form-control" placeholder="Telf..." value="<?php echo (!empty($dominio)) ? ($dominio->telefono != '' ? $dominio->telefono : "") : '';?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Dirección</label>
                                                <input type="text" id="direccion_dominio" name="direccion_dominio" class="form-control" placeholder="Dirección" value="<?php echo (!empty($dominio)) ? ($dominio->direccion != '' ? $dominio->direccion : "") : '';?>">
                                            </div>
                                        </div>
                                       
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Departamento</label>
                                                <select name="departamento_dominio" id="departamento_dominio" class="form-control" onChange="javascript:change_provincia('departamento_dominio','provincia_dominio')">
                                                    <option value="0">Selecciona tu departamento</option>
                                                    <?php foreach ($departamentos as $departamento):  ?>
                                                    
                                                        <?php 
                                                        if(!empty($departamentos)): 
                                                            if(!empty($dominio)): 
                                                                if ($departamento->cod_ubigeo == $dominio->id_departamento) : ?>
                                                                    <option value="<?php echo $departamento->cod_ubigeo; ?>" selected><?php echo $departamento->nombre; ?></option>                         
                                                                    <?php else: ?>
                                                                    <option value="<?php echo $departamento->cod_ubigeo; ?>"><?php echo $departamento->nombre; ?></option>   
                                                                    <?php endif;?>
                                                                
                                                                <?php else: ?>
                                                                    <option value="<?php echo $departamento->cod_ubigeo; ?>"><?php echo $departamento->nombre; ?></option>   
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?> 
                                                </select>     

                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Provincia</label>
                                                <select name="provincia_dominio" id="provincia_dominio" class="form-control" onChange="javascript:change_distrito('provincia_dominio','distrito_dominio')">
                                                    <option value="0">Selecciona tu provincia</option>
                                                    <?php if(!empty($provincias_dominio)): ?>
                                                                    <?php foreach ($provincias_dominio as $provincia):  ?>
                                                       
                                                                    <?php 
                                                                            if ($provincia->cod_ubigeo == $dominio->id_provincia) : ?>
                                                                                <option value="<?php echo $provincia->cod_ubigeo; ?>" selected><?php echo $provincia->nombre; ?></option>                         
                                                                        <?php else: ?>
                                                                                <option value="<?php echo $provincia->cod_ubigeo; ?>"><?php echo $provincia->nombre; ?></option>   
                                                                        <?php endif;?>
                                                                    <?php endforeach; ?> 

                                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">Distrito</label>
                                                <select name="distrito_dominio" id="distrito_dominio" class="form-control">
                                                        <option value="0">Selecciona tu distrito</option>
                                                        <?php if(!empty($distritos_dominio)): ?>
                                                                    <?php foreach ($distritos_dominio as $distrito):  ?>
                                                       
                                                                    <?php 
                                                                            if ($distrito->cod_ubigeo == $dominio->id_distrito) : ?>
                                                                                <option value="<?php echo $distrito->cod_ubigeo; ?>" selected><?php echo $distrito->nombre; ?></option>                         
                                                                        <?php else: ?>
                                                                                <option value="<?php echo $distrito->cod_ubigeo; ?>"><?php echo $distrito->nombre; ?></option>   
                                                                        <?php endif;?>
                                                                    <?php endforeach; ?> 

                                                                <?php endif; ?>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="form-actions text-right">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Guardar Web</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                        </form>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->
                <?php echo Modules::run("template/show_footer");?> 
                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
        </div>
        
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
 
</div>

<script>
    // conseguir las provincias de acuerdo al departamento
    function change_provincia(campo_departamento, campo_provincia)
    {
       

        id_departamento = $("#"+campo_departamento).val();
        $("#"+campo_provincia).find('option').remove().end();
        $("#"+campo_provincia).append('<option value="">Selecciona tu provincia</option>').val('');
        //$("#"+campo_provincia).prop('required', false); // por default es false

        if(id_departamento != "")
        {
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>/negocios/get_provincias',
            data: { 'id_departamento': id_departamento},
            dataType: 'json',
            success: function(data){
            if(data != false || data != ''){
                //$("#"+campo_provincia).prop('required', true);
                $.each(data, function(i, d){
                    $("#"+campo_provincia).append('<option value="' + d.cod_ubigeo + '">'
                                        + d.nombre + '</option>');
                })
                }
            },
            error: function(msg){
                
                }
            })
        }
       
    }
    

    // conseguir las provincias de acuerdo al departamento
    function change_distrito(campo_provincia, campo_distrito)
    {

        id_provincia = $("#"+campo_provincia).val();
     
        $("#"+campo_distrito).find('option').remove().end();
        $("#"+campo_distrito).append('<option value="">Selecciona tu distrito</option>').val('');
        //$("#"+campo_distrito).prop('required', false); // por default es false

        if(id_provincia != "")
        {
        $.ajax({
            type: 'POST',
            url: '<?php echo base_url();?>/negocios/get_distritos',
            data: { 'id_provincia': id_provincia},
            dataType: 'json',
            success: function(data){
            if(data != false || data != ''){
                //$("#"+campo_distrito).prop('required', true);
                $.each(data, function(i, d){
                    $("#"+campo_distrito).append('<option value="' + d.cod_ubigeo + '">'
                                        + d.nombre + '</option>');
                })
                }
            },
            error: function(msg){
                
                }
            })
        }
       
    }

    // Eliminar logo
    $('.eliminar_logo').click(function(){
        $("#logo_negocio").remove();
        $(".eliminar_logo").remove();
        $("#content_imagen_logo").append('<input type="file" name="logo" id="logo" class="form-control" required="" accept="image/x-png"> <div class="help-block"></div></div><small class="form-control-feedback"> Solo imágenes PNG* </small>');
    });

    // Eliminar logo
    $('.eliminar_banner').click(function(){
        $("#banner_negocio").remove();
        $(".eliminar_banner").remove();
        $("#content_imagen_banner").append('<input type="file" name="imagen_banner"  id="imagen_banner" class="form-control" required="" accept="image/jpeg"> <div class="help-block"></div><small class="form-control-feedback">Formato JPG, orientación horizontal, tamaño mínimo 1800px de ancho* </small>');
    });

    

    // Plantilla
    $('#id_plantilla').change(function(){
        var id_plantilla = $('#id_plantilla').val();
        var src_plantilla ="../images/plantillas/"+id_plantilla+".jpg";
        
        $('#imagen_plantilla').attr('src', src_plantilla);
    });

    // Add Rol
    let  productos = {};
    var i = 1;
    var imagen = '';
    var nombre_imagen = '';

    $('#addProducto').click(function(e){
        e.preventDefault();
        var parent = $('#detalleProducto');
        var nombre = $('#nombre_producto').val();
        var codigo = $('#codigo_producto').val();
        var descripcion = $('#descripcion_producto').val();
        var categoria = $('#categoria_producto').val();
        var precio = $('#precio_producto').val();
        
        if(nombre != '' && codigo !='' && precio != '' & imagen != ''){

            $('#listaProducto table tbody').append('<tr><td width="50"><i class="ti-arrows-vertical"></i></td><td style="width:50px;"><span class="round"><img src="'+imagen+'" alt="user" width="50"></span></td><td>'+nombre+'</td><td>S/ '+precio+'</td><td><button type="button" class="btn btn-warning btn-circle"><i class="fa fa-times"></i> </button></td></tr>');
            $('detalleProducto input').val('');
            productos[i] = {nom :nombre,cat :categoria,pre :precio,des :descripcion,cod :codigo,img:nombre_imagen}

            // Limpiar datos
            $('#nombre_producto').val('');
            $('#codigo_producto').val('');
            $('#descripcion_producto').val('');
            $('#categoria_producto').val('');
            $('#precio_producto').val('');
            $('#imagen_producto').val('');
            
            save_image(imagen, nombre, i);
            $('#productosNegocio').val(JSON.stringify(productos));

            i++;
        }
        else
        {
            alert("Completar la información de los productos");
        }
    });

    // Envíos
    
    let  envios = {};
    var i = 9999;

    $('#addEnvio').click(function(e){
        e.preventDefault();
        var departamento = $('#departamento_envio').val();
        var provincia = $('#provincia_envio').val();
        var distrito = $('#distrito_envio').val();
        var costo = $('#costo_envio').val();
        var tiempo = $('#tiempo_envio').val();
        var nombre_distrito = $('#distrito_envio option:selected').text();
        var nombre_departamento = $('#departamento_envio option:selected').text();
        var envioId = i;
        
        if(provincia != '' && distrito !='' && tiempo != '' & costo != '' & departamento !=''){

            $('#listaEnvio table tbody').append('<tr><td width="30" class="envio_id" data-envio-id='+envioId+'><i class="ti-arrows-vertical"></i></td><td style="width:50px;">'+nombre_departamento+'</td><td>'+nombre_distrito+'</td><td> <button type="button" class="btn btn-warning btn-circle deleteEnvio" id="deleteEnvio'+i+'"><i class="fa fa-times"></i> </button></td> </tr>');
            $('#detalleEnvio input').val('');
            envios[i] = {dep :departamento,pro :provincia,dis :distrito,cos :costo,tie :tiempo}

            // Limpiar datos
            $('#departamento_envio').val('');
            $('#provincia_envio').val('');
            $('#distrito_envio').val('');
            $('#costo_envio').val('');
            $('#tiempo_envio').val('');

            i++;
            $('#enviosNegocio').val(JSON.stringify(envios));
        }
        else
        {
            alert("Completar la información de los envíos");
        }
    });


    // Read image
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                imagen = e.target.result;
            };

            reader.readAsDataURL(input.files[0]);

            
        }
    }

    function save_image(imagen, nombre, key)
    {
        // Bloquear botón
        $("addProducto").attr("disabled", true);
        // Save image
        $.ajax({
            method: "POST",
            url: "save_image",
            data:{
                base64: imagen,
                nombreProd: nombre
            },
            })
            .done(function( nom_img ) {
                console.log( "Data Saved: " + nom_img );
                var lista = $('#productosNegocio').val();
          
                var obj = JSON.parse(lista);
                for(var i in obj) {
                    if(i == key)
                    {
                        obj[i].img = nom_img;
                        $('#productosNegocio').val(JSON.stringify(obj));
                        $("addProducto").attr("disabled", false);
                        break;
                    }
                    
                }
                
                

            })
            .fail(function() {
                console.log( "error" );
                alert("No se pudo subir la imagen del producto");
                
            })
            .always(function() {
                console.log( "complete" );
            });
    }

    // Delete envío
    $(document).on('click', '.deleteEnvio', function(e){
        e.preventDefault();
        var aId = e.target.parentElement.id;
        var tag = document.getElementById(aId);
        var enviosIdKey = $(tag).closest('tr').children().first().data('envio-id');
        delete envios[enviosIdKey];
        $('#enviosNegocio').val(JSON.stringify(envios));
        tag.closest('tr').remove();

    });

    // Contador de caracteres
    $('#descripcion_producto').keyup(function () {
        console.log("escribiendo descripcion")
        var max = 250;
        var len = $(this).val().length;
        if (len >= max) {
            $('#charNumProducto').text(' llegaste al límite');
        } else {
            var char = max - len;
            $('#charNumProducto').text(char + ' restantes');
        }
    });

    //Botones
    $('.btnNext').click(function() {
        $('.nav-tabs .active').parent().next('li').find('a').trigger('click');
    });

    $('.btnPrevious').click(function() {
        $('.nav-tabs .active').parent().prev('li').find('a').trigger('click');
    });

</script>

</body>
</html>
