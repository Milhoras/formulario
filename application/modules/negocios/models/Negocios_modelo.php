<?php defined('BASEPATH')OR exit('No direct script access allowed');

class Negocios_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'negocios';
	}

	function get_by_ruc($ruc)
	{
		$this->db->from($this->__tabla);
		$this->db->where('estado', '1');
		$this->db->where('ruc', $ruc);

		return $this->db->get()->row();
	}
	
	function get_by_id($id_negocio)
	{
		$this->db->select('*,negocios.nombre as nombre, bancos.nombre as nombre_banco ');
		$this->db->from($this->__tabla);
		$this->db->join('bancos', 'bancos.id_banco = negocios.id_banco', 'left outer');
		$this->db->where('id_negocio', $id_negocio);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->row();
		}

		return FALSE;
	}

	function get_destacados($id_negocio)
	{
		$this->db->from('destacados');
		$this->db->where('id_negocio', $id_negocio);

		return $this->db->get()->result();
	}

	function get_departamentos()
	{
		$nids_departamentos = array('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25');

		$this->db->from('ubigeos');
		$this->db->where('estado', '1');
		$this->db->where_in('cod_ubigeo', $nids_departamentos);


		$query = $this->db->get();
		$lista = '';

		if($query->num_rows() > 0)
		{
			$departamentos = $query->result();
			return $departamentos;
		}
	}

	function get_provincias($id_departamento)
	{
		$cantidad = strlen($id_departamento) + 2;
		$this->db->from('ubigeos');
		$this->db->where('estado', '1');
		$this->db->where('CHAR_LENGTH(cod_ubigeo)', $cantidad);
		
		$this->db->like('cod_ubigeo', $id_departamento); 
		//$this->db->where('cod_ubigeo', $id_departamento);
		
		$query = $this->db->get();
		//var_dump($query->result());

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return '';
	}

	function get_distritos($id_provincia)
	{
		$cantidad_min = strlen($id_provincia) + 2;
		$cantidad_max = strlen($id_provincia) + 3;
		$this->db->from('ubigeos');
		$this->db->where('estado', '1');
		$this->db->where('CHAR_LENGTH(cod_ubigeo) >=', $cantidad_min);
		$this->db->where('CHAR_LENGTH(cod_ubigeo) <=', $cantidad_max);
		$this->db->like('cod_ubigeo', $id_provincia); 

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return '';
	}

	function get_bancos()
	{
		$this->db->from('bancos');
		$this->db->where('estado', '1');		
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return 0;
	}

	function update_negocio($id_negocio)
	{
		$nombre = $this->input->post('nombre');
		$telefono = $this->input->post('telefono');
		$email = $this->input->post('email');
		$direccion = $this->input->post('direccion');
		$id_departamento = $this->input->post('id_departamento');
		$id_provincia = $this->input->post('id_provincia');
		$id_distrito = $this->input->post('id_distrito');
		$numero_cuenta = $this->input->post('numero_cuenta');
		$numero_cci = $this->input->post('numero_cci');
		$id_banco = $this->input->post('id_banco');
		$redsocial_facebook = $this->input->post('redsocial_facebook');
		$redsocial_twitter = $this->input->post('redsocial_twitter');
		$redsocial_instagram = $this->input->post('redsocial_instagram');
		$redsocial_youtube = $this->input->post('redsocial_youtube');
		$logo = $this->input->post('logo');
		$imagen_banner = $this->input->post('imagen_banner');
		$texto_banner = $this->input->post('texto_banner');
		$descripcion = $this->input->post('descripcion');
		$local = $this->input->post('local');

		// Destacados
		$titulo_1 = $this->input->post('titulo_1');
		$texto_1 = $this->input->post('texto_1');
		$titulo_2 = $this->input->post('titulo_2');
		$texto_2 = $this->input->post('texto_2');
		$titulo_3 = $this->input->post('titulo_3');
		$texto_3 = $this->input->post('texto_3');

		// Plantilla
		$id_plantilla = $this->input->post('id_plantilla');

		// Producto
		$nombre_producto = $this->input->post('nombre_producto[]');
		$codigo_producto = $this->input->post('codigo_producto[]');
		$categoria_producto = $this->input->post('categoria_producto[]');
		$precio_producto = $this->input->post('precio_producto[]');
		$descripcion_producto = $this->input->post('descripcion_producto[]');
		//$imagen_producto = $this->input->post('imagen_producto[]');

		// zonas de reparto
		$envio_costo = $this->input->post('envio_costo');
		$envio_provincia = $this->input->post('envio_provincia');
		$envio_distrito = $this->input->post('envio_distrito');
		$envio_tiempo_entrega = $this->input->post('envio_tiempo_entrega');
		$envio_departamento = $this->input->post('envio_departamento');

		// Dominio
		$dominio = $this->input->post('dominio');
		$nombre_dominio = $this->input->post('nombre_dominio');
		$tld_dominio = $this->input->post('tld_dominio');
		$numero_dominio = $this->input->post('numero_dominio');
		$ruc_dni = $this->input->post('ruc_dni');
		$razon_social = $this->input->post('razon_social');
		$tipo_razon_nombre = $this->input->post('tipo_razon_nombre');
		$email_dominio = $this->input->post('email_dominio');
		$telefono_dominio = $this->input->post('telefono_dominio');
		$direccion_dominio = $this->input->post('direccion_dominio');
		$distrito_dominio = $this->input->post('distrito_dominio');
		$provincia_dominio = $this->input->post('provincia_dominio');
		$departamento_dominio = $this->input->post('departamento_dominio');

		$fecha_actual = date('Y-m-d H:i:s');

		if($nombre != '')
		{
			$datos_negocio = array(
				'nombre' => $nombre,
				'telefono' => $telefono,
				'email' => $email,
				'direccion' => $direccion,
				'id_departamento' => $id_departamento,
				'id_provincia' => $id_provincia,
				'id_distrito' => $id_distrito,
				'numero_cuenta' => $numero_cuenta,
				'numero_cci' => $numero_cci,
				'id_banco' => $id_departamento,
				'redsocial_facebook' => $redsocial_facebook,
				'redsocial_twitter' => $redsocial_twitter,
				'redsocial_instagram' => $redsocial_instagram,
				'redsocial_youtube' => $redsocial_youtube,
				'descripcion' => $descripcion,
				'fecha_registro' => $fecha_actual,
				'id_plantilla' => $id_plantilla,
				'local' => $local,
				'texto_banner' => $texto_banner
			);	
		
			$this->db->where('id_negocio', $id_negocio);
			$this->db->update($this->__tabla, $datos_negocio);

			// Destacados
			// eliminar destacados existentes
			$this->db->delete('destacados', array('id_negocio' => $id_negocio));

			// Insertar datos
			for($i = 1; $i <= 3; $i++) 
			{
				if(!empty(${"titulo_" . $i}))
				{
					$datos_destacados = array(				
						'titulo' => ${"titulo_" . $i},
						'descripcion' => ${"texto_" . $i},
						'id_negocio' => $id_negocio
					);	
					$this->db->insert('destacados', $datos_destacados);
				}
				
			}		

			// Negocio
			if(!empty($_FILES['logo']['name']))
			{
				$logo = $this->save_imagen("logo",$_FILES['logo']['name'], './images/negocios/');

				$datos_negocio = array(
					'logo' => $logo
				);	
			
				$this->db->where('id_negocio', $id_negocio);
				$this->db->update($this->__tabla, $datos_negocio);
				
			}
			
			if(!empty($_FILES['imagen_banner']['name']))
			{
				$imagen_banner = $this->save_imagen("imagen_banner",$_FILES['imagen_banner']['name'], './images/negocios/');

				$datos_negocio = array(
					'imagen_banner' => $imagen_banner
					
				);	
			
				$this->db->where('id_negocio', $id_negocio);
				$this->db->update($this->__tabla, $datos_negocio);
				
			}			
		
			// Productos
			$productos = $this->input->post('productosNegocio', TRUE);
			if(!empty($productos))
			{
				
				$productos = json_decode($productos, TRUE);
								
				if(!empty($productos)){
					foreach($productos as $order => $producto)
					{
						$datos_producto = array(				
							'nombre' => $producto['nom'],
							'codigo' => $producto['cod'],
							'categoria' => $producto['cat'],
							'precio' => $producto['pre'],
							'descripcion' => $producto['des'],
							'imagen' => $producto['img'],
							'id_negocio' => $id_negocio,
						);
						$this->db->insert('productos', $datos_producto);
					}
				}
			}

			// envios
			$envios = $this->input->post('enviosNegocio', TRUE);
			if(!empty($envios))
			{
				$envios = json_decode($envios, TRUE);
				
				if(!empty($envios)){
					foreach($envios as $order => $envio)
					{
						$datos_envio = array(				
							'id_negocio' => $id_negocio,
							'id_departamento' => $envio['dep'],
							'id_provincia' => $envio['pro'],
							'id_distrito' => $envio['dis'],
							'tiempo' => $envio['tie'],
							'costo' => $envio['cos']
						);
						$this->db->insert('envios', $datos_envio);
					}
				}
			}

			
			


			// Dominio
			$id_dominio = $this->get_id_dominio_by_negocio($id_negocio);
			$datos_dominio = array(
				'dominio' => $dominio,
				'nombre' => $nombre_dominio,
				'tld' => $tld_dominio,
				'numero' => $numero_dominio,
				'ruc_dni' => $ruc_dni,
				'razon_social' => $razon_social,
				'tipo_razon_nombre' => $tipo_razon_nombre,
				'email' => $email_dominio,
				'telefono' => $telefono_dominio,
				'direccion' => $direccion_dominio,
				'id_departamento' => $departamento_dominio,
				'id_provincia' => $provincia_dominio,
				'id_distrito' => $distrito_dominio,
				'id_negocio' =>$id_negocio
			);	
		
			if($id_dominio > 0)
			{
				$this->db->where('id_dominio', $id_dominio);
				$this->db->where('id_negocio', $id_negocio);
				$this->db->update('dominios', $datos_dominio);
			}
			else
			{
				$this->db->insert('dominios', $datos_dominio);
			}
			

			return TRUE;
		}
		return FALSE;	

	}

	function save_imagen($file, $imagen, $ruta_imagen)
	{
		//Nombre de la imagen
		$filename = $imagen;
		$nombre = str_replace(' ', '-', $filename);
		$rand = substr(md5(microtime()),rand(0,26));
		$ext = substr($filename, strrpos($filename, '.'));
		$nombre_nuevo = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;

		
		$config['upload_path'] = $ruta_imagen;
		$config['allowed_types'] = 'jpg|png|jpeg|JPG';
		$config['max_size'] = '6000';
		$config['max_width'] = '6000';
		$config['max_height'] = '6000';
		$config['file_name'] = $nombre_nuevo;

		// subir a la BD y al bucket
		$this->load->library('upload',$config);

		// Subir imagen
		if ($this->upload->do_upload($file)) {

			
			$error = $this->upload->display_errors();
			$file_info = $this->upload->data();	
			
			return $nombre_nuevo;		
		} 
		else
			return '';

		
	}

	function get_plantillas()
	{
		$this->db->from('plantillas');
		$this->db->order_by('nombre');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return 0;
	}

	function get_id_dominio_by_negocio($id_negocio)
	{
		$this->db->select('id_dominio');
		$this->db->from('dominios');
		$this->db->where('id_negocio', $id_negocio);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->row()->id_dominio;
		}
		return 0;
	}

	function get_dominio_by_negocio($id_negocio)
	{
		$this->db->from('dominios');
		$this->db->where('id_negocio', $id_negocio);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		return '';
	}

	function get_envios_by_negocio($id_negocio)
	{
		$this->db->select('envios.id_departamento, envios.id_provincia, envios.id_distrito, u.nombre as departamento, i.nombre as distrito, id_envio');
		$this->db->from('envios');
		$this->db->where('id_negocio', $id_negocio);
		$this->db->join('ubigeos as u', 'u.cod_ubigeo = envios.id_departamento', 'left outer');
		$this->db->join('ubigeos as i', 'i.cod_ubigeo = envios.id_distrito', 'left outer');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return '';
	}

	function get_productos_by_negocio($id_negocio)
	{
		$this->db->select('productos.nombre, productos.imagen, productos.precio, id_producto');
		$this->db->from('productos');
		$this->db->where('id_negocio', $id_negocio);
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		return '';
	}



}
