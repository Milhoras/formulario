<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Negocios extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('Negocios_modelo'));
		$this->load->library('pagination');
	}

	
	/**
	 * Cambia el estado de la negocio a '2'
	 * @param int $id_negocio código identificador de la negocio
	 * @access private
	 */
	function eliminar_negocio($id_negocio)
    {       
    	if(!empty($id_negocio))
    	{
    		$success = $this->Negocios_modelo->delete_negocio($id_negocio);
	        $mensaje = $success ? 'negocio eliminada' : 'Hubo un error al eliminar la negocio. Por favor inténtalo de nuevo';

	        $this->session->set_flashdata('success', $mensaje);        
    	}
    	redirect("Negocios");
       
    }

   	/**
	 * Devuelve la página de registro de negocios
	 * La vista incluye la lista de radios en estado activo (código: 1)
	 * @access private
	 */
    function formulario_registro_negocio()
    {
    	$data['radios'] = $this->Radios_modelo->get_all();
    	$this->load->view('registrar', $data);
    }

    /**
	 * Registra la información de la negocio en la BD
	 * Muestra un mensaje de confirmación o error según sea el caso
	 * En caso de que el registro se haya almacenado muestra el subhome de las mecánicas
	 * En caso de un problema con el registro lo redirecciona a la lista de negocios
	 * @access private
	 */
    function registrar_negocio()
	{
		if($_POST)
		{
			$success = $this->Negocios_modelo->insert_negocio();		
			$mensaje = $success ? 'negocio creada' : 'Hubo un error al crear la negocio. Por favor inténtalo de nuevo';

	        $this->session->set_flashdata('success', $mensaje);  
	        redirect("mecanicas/".$success);      	
		}
		redirect("Negocios");
		
	}

	/**
	 * Devuelve la página de edición de negocio
	 * La vista incluye la información de la negocio: id_negocio, nombre, fecha_inicio, fecha_fin, id_radio
	 * La vista incluye la lista de radios en estado activo (código: 1)
	 * En caso el id de la negocio no exista lo redirecciona a la lista de negocios
	 * @param int $id_negocio código identificador de la negocio
	 * @access private
	 */
	function editar_negocio($id_negocio)
	{
		if(!empty($id_negocio))
		{
			$data['id_negocio'] = $id_negocio;
			$data['departamentos'] = $this->Negocios_modelo->get_departamentos();
			$data['bancos'] = $this->Negocios_modelo->get_bancos();
			$data['plantillas'] = $this->Negocios_modelo->get_plantillas();

			$negocio = $this->Negocios_modelo->get_by_id($id_negocio);
			$provincias = '';
			$distritos = '';

			if(!empty($negocio->id_departamento))
			{
				$provincias = $this->Negocios_modelo->get_provincias($negocio->id_departamento);
				$distritos = $this->Negocios_modelo->get_distritos($negocio->id_provincia);
			}
	
			$data['provincias'] = $provincias;
			$data['distritos'] = $distritos;


			// DESTCADOS
			$data['destacados'] = $this->Negocios_modelo->get_destacados($id_negocio);

			// DOMINIOS
			$dominio = $this->Negocios_modelo->get_dominio_by_negocio($id_negocio);
			$data['dominio'] = $dominio;
			if(!empty($dominio))
			{
				
				$data['provincias_dominio'] = $this->Negocios_modelo->get_provincias($dominio->id_departamento);
				$data['distritos_dominio'] = $this->Negocios_modelo->get_distritos($dominio->id_provincia);
			}
			

			// ENVIOS
			$envios = $this->Negocios_modelo->get_envios_by_negocio($id_negocio);
			$data['envios'] = $envios;
		

			// PRODUCTOS
			$data['productos'] = $this->Negocios_modelo->get_productos_by_negocio($id_negocio);;


			if(!empty($negocio))
			{
				$data['negocio'] = $negocio;
				$this->load->view('editar', $data);
			}
			else
			{
				redirect("negocios");
			}
			
		}
		else
		{
			redirect("negocios");
		}
		
	}

	/**
	 * Actualiza la información de la negocio
	 * Muestra un mensaje de confirmación o error según sea el caso
	 * Al finalizar muestra el subhome de las negocios
	 * @param int $id_negocio código identificador de la negocio
	 * @access private
	 */
	function actualizar_negocio($id_negocio)
	{
		if($_POST)
		{
			$success = $this->Negocios_modelo->update_negocio($id_negocio);
			if($success)
			{
				$mensaje = 'Datos actualizados';
				$this->session->set_flashdata('success', $mensaje);	
			}
			else
			{
				$mensaje = 'No se pudieron almacenar los datos';
				$this->session->set_flashdata('error', $mensaje);	
			}
			
			redirect("negocios/".$id_negocio);
		}
	}

	function get_provincias()
	{
		if($_POST)
        { 
    		$id_departamento = $this->input->post('id_departamento');
            $id_provincia = $this->Negocios_modelo->get_provincias($id_departamento);

            if(!empty($id_provincia))
            {
                echo json_encode($id_provincia);
            }
            else
            {
                echo 0;
            }
        }
        else
        {
            echo 0;
        }
	}

	function get_distritos()
	{
		if($_POST)
        { 
    		$id_provincia = $this->input->post('id_provincia');
            $id_distrito = $this->Negocios_modelo->get_distritos($id_provincia);

            if(!empty($id_distrito))
            {
                echo json_encode($id_distrito);
            }
            else
            {
                echo 0;
            }
        }
        else
        {
            echo 0;
        }
	}

	function save_image()
	{
		if($_POST)
        { 
    		$base64 = $this->input->post('base64');
			$filename = $this->input->post('nombreProd');

            if(!empty($base64))
            {
	
				$nombre = str_replace(' ', '-', $filename);
				$rand = substr(md5(microtime()),rand(0,26),5);
				$ext = substr($filename, strrpos($filename, '.'));
				$nombre_nuevo = strtolower(url_title(convert_accented_characters($nombre.'-'.$rand))).$ext;

                $base_to_php = explode(',', $base64);
				$data = base64_decode($base_to_php[1]);
				
				$extension = explode('/', mime_content_type($base64))[1];
				$filepath = "images/productos/".$nombre_nuevo.".".$extension;
				file_put_contents($filepath,$data);

				echo $nombre_nuevo.".".$extension;

            }
            else
            {
                echo 0;
            }
        }
        else
        {
            echo 0;
        }
	}

	
	


}