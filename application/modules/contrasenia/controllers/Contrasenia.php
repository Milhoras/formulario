	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contrasenia extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('validador/Validador_modelo', 'Contrasenia_modelo'));
	}

	function olvido_contrasenia()
	{
		$this->load->view('olvido_contrasenia');
	}

	function mail_cambiar_contrasenia()
	{
		$email = $this->input->post('email');
		$msg = '';

		$credential = $this->Validador_modelo->validar_email($email);
		if($credential)
		{

			//Config de email
			$config['protocol'] = "smtp";
			$config['smtp_host'] = "ssl://smtp.gmail.com";
			$config['smtp_port'] = "465";
			$config['smtp_user'] = "digital.crpradio@gmail.com"; 
			$config['smtp_pass'] = "K(9Z6M6q5;h3Rs79";
			$config['charset'] = "utf-8";
			$config['mailtype'] = "html";
			$config['newline'] = "\r\n";

			$cadena = $email.rand(1,time()).date('Y-m-d');
  			$token = sha1($cadena);

  			$url = $token.'/'.$email;
  			// $url = $this->Contrasenia_modelo->encode_this($url);
  			$new_url = base_url().'cambiar_contrasenia/'.$url;
  			$msg = '<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"><div style="background:#ffffff; width:600px; margin:0 auto 0;"><table id="Tabla_01" width="600" height="547" border="0" cellpadding="0" cellspacing="0">
			<tr><td width="600" height="308"><img src="http://d2h9150wp9tgke.cloudfront.net/rubik/mailing/mailing_01.jpg"  alt="Mailing restablecer" width="600" height="308" style="border:none; vertical-align:bottom; display: block;"/></td></tr><tr><td width="600" height="72"><a href="'.$new_url.'"><img src="http://d2h9150wp9tgke.cloudfront.net/rubik/mailing/mailing_02.jpg" alt="Mailing restablecer" style="border:none; vertical-align:bottom; display: block;"/></a>
				</td></tr><tr><td width="600" height="167"><img src="http://d2h9150wp9tgke.cloudfront.net/rubik/mailing/mailing_03.jpg" alt="Mailing restablecer" width="600" height="167" style="border:none; vertical-align:bottom; display: block;"/></td>
			</tr></table></div></body>';

			// // Se debe enviar un email con un token
			$this->load->library('email', $config);
			$this->email->from('digital@crp.pe', 'Rubik');
			$this->email->to($email);
			$this->email->cc('hbravo@crp.pe');	
			$this->email->subject('solicitaste cambio de contraseña');
			$this->email->message($msg);		
			$this->email->send();

  			$registro = $this->Contrasenia_modelo->register_token($token, $email);

  			if($registro)
  			{
  				$mensaje = 'Se ha enviado el email para que puedas cambiar la contraseña.';
  				$this->session->set_flashdata('success_tipo', '2');		
				$this->session->set_flashdata('success', $mensaje);				
				
  			}			
			redirect("olvido_contrasenia");		

		}
		else
		{
			$mensaje = 'El usuario no existe';
			$this->session->set_flashdata('error', $mensaje);
			redirect("login");
		}
		
	}

	function cambiar_contrasenia($token, $email)
	{
		$credential = $this->Validador_modelo->validar_token_email($token, $email);
		if(!empty($credential))
		{
			$data['email'] = $email;
			$data['token'] = $token;
			$this->load->view('cambiar_contrasenia', $data);
		}
		else
		{
			redirect("login");
		}	
	}

	function actualizar_contrasenia()
	{
		if($_POST)
		{
			$email = $this->input->post('email');
			$password = $this->input->post('nueva_contrasenia');

			$registro = $this->Contrasenia_modelo->actualizar_password($email, $password);

			if($registro)
			{
				$mensaje = 'Contraseña actualizada';
				$this->session->set_flashdata('success', $mensaje);
				$this->session->set_flashdata('success_tipo', '2');
				redirect("login");
				
			}
		}
		redirect("login");

	}
}