<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="/assets/images/favicon.png" type="image/png">-->

  <title>RUBIK</title>

  <!-- css -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

  <!-- js -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.12.4.min.js"></script>

  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/library_validation.js"></script>
  <script src="<?php echo base_url();?>assets/js/validation.js"></script>

</head>

<body>

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <section id="core-wrapp">
    
    <div class="content-wrapp container">
  
      
      <?php if($this->session->flashdata('error') != ''): ?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php endif; ?>

        
      <div class="login-wrapp"> 

        <div class="row"> 

            <div class="login-head">
                <h1>Rubik</h1>
                <h4 class="txt-center">RECUPERA TU CONTRASEÑA</h4>
            </div> <!-- fin login-head -->

            <div class="login-body">
              

              <form action="<?php echo base_url();?>contrasenia/actualizar_contrasenia" method="post" onsubmit="return validar_cambiar_contrasenia();" class="form-wrapp">


                <div class="col-xs-12 col-sm-8 form-line">
                  
                  <div class="input-group">

                    <input type="text" class="form-control" placeholder="Email" name="email" id="email" value="<?php echo $email; ?>" readonly>
                  </div>
                </div> <!-- fin form-line -->



                <div class="col-xs-12 col-sm-8 form-line">
                  
                  <div class="input-group">
                    <label class="txt-gen label-gen">Tu nueva contraseña:</label>
                    <input type="password" class="form-control" placeholder="Escribe tu nueva contraseña" name="nueva_contrasenia" id="nueva_contrasenia">
                  </div>
                </div> <!-- fin form-line -->



                <div class="col-xs-12 col-sm-8 form-line">
                  
                  <div class="input-group">
                    <label class="txt-gen label-gen">Confirma tu nueva contraseña:</label>
                    <input type="password" class="form-control" placeholder="Escribe la confirmación de tu nueva contraseña" name="repetir_nueva_contrasenia" id="repetir_nueva_contrasenia">
                  </div>
                </div> <!-- fin form-line -->
                
                <br>

              
                <div class="center-gen">
                  <button type="submit" class="btn-gen" id="">guarda tu nueva contraseña</button>
                </div>
                <br><br>

              </form>

              <hr class="invisible">

            </div> <!-- fin login-body -->

        </div> <!-- fin row -->


      </div> <!-- fin login-wrapp -->

      

    </div> <!-- fin content-wrapp -->

  </div><!-- fin core-wrapp -->


  

</body>
</html>
