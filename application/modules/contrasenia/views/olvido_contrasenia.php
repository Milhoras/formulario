<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="/assets/images/favicon.png" type="image/png">-->

  <title>RUBIK</title>

  

  <!-- css -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.min.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


  <!-- js -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.12.4.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/library_validation.js"></script>
  <script src="<?php echo base_url();?>assets/js/validation.js"></script>
</head>



<body>

  <div class="sign-overlay"></div>
  <div class="signpanel"></div>

  <section id="core-wrapp">
    
    <div class="content-wrapp container">
  
      
      <?php if($this->session->flashdata('error') != ''): ?>

        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
        </div>

        <?php else: ?>
            <?php if($this->session->flashdata('success') != ''):?>
                <?php if($this->session->flashdata('success_tipo') == '2'):?>
                  <script type="text/javascript">
                    $(document).ready(function() {
                      $("#modal_confirmacion").modal('show');
                    });
                  </script>

                <?php else: ?>
                  <div class="alert alert-success">
                    <?php echo $this->session->flashdata('success');?>
                  </div>
            <?php endif; ?>
        <?php endif; ?>

      <?php endif; ?>

        
      <div class="login-wrapp"> 

        <div class="row"> 

            <div class="login-head">
                <h1>Rubik</h1>
                <h4 class="txt-center">RECUPERA TU CONTRASEÑA</h4>
            </div> <!-- fin login-head -->

            <div class="login-body">
              <p class="txt-gen txt-instru">Si te has olvidado la contraseña, te podemos ayudar enviándote un correo a la dirección con la que creaste tu cuenta:</p>

              <form action="<?php echo base_url();?>contrasenia/mail_cambiar_contrasenia" method="post" onsubmit="return validar_olvido_contrasenia();" class="form-wrapp">


                <div class="col-xs-12 col-sm-8 form-line">
                  
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Escribe tu cuenta" name="email" id="email" required>
                  </div>
                </div> <!-- fin form-line -->
                
                <br>

              
                <div class="center-gen">
                  
                  <a href="<?php echo base_url();?>" class="btn-none btn-none-mob" id="" data-dismiss="modal" aria-label="Close">Cancelar</a>
                  <button type="submit" class="btn-gen" id="">restablece tu contraseña</button>
                </div>
                <br><br>

              </form>

              <hr class="invisible">

            </div> <!-- fin login-body -->

        </div> <!-- fin row -->


      </div> <!-- fin login-wrapp -->

      

    </div> <!-- fin content-wrapp -->

  </div><!-- fin core-wrapp -->



  <!-- MODAL DE RECUPERAR CONTRASEÑA -->
  <div class="modal fade" id="modal_confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">

      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
          
        </div> <!-- fin modal-header -->

        <div class="modal-body">
          <p class="txt-gen modal-title">RECUPERA TU CONTRASEÑA</p>
          <h3 class="txt-center">¡Listo, revisa tu correo!</h3>
          <p class="txt-gen modal-txt reset-bottom">¡Listo, revisa tu correo!</p>
          <p class="txt-gen modal-txt reset-bottom">Si el correo no llega, revisa  la carpeta “spam”.</p>
          <!-- <p class="txt-gen modal-txt reset-bottom">En caso no haber recibido el correo, reenvíalo haciendo clic <a href="#" class="here">aquí</a>.</p> -->

         

          <div class="center-gen">
            <a href="<?php echo base_url(); ?>login" class="btn-gen" id="">Volver</a>
          </div>
        </div> <!-- fin modal-body -->
        
      </div>

    </div>

  </div>


</body>
</html>
