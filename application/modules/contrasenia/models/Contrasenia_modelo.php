<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contrasenia_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'usuarios';
		$this->load->library('form_validation');
	}

	/*
	 * @access public
	 * @param String $token token de la cuenta de Emblue
	 * @param String $email email del usuario
	 * @return Boolean true
	 */
	function register_token($token, $email)
	{
		$array = array(
			'token' => $token,
			'estado' => '2'
		);

		$this->db->where('email', $email);
		$this->db->update('usuarios', $array);

		return TRUE;
	}

	/*
	 * @access public
	 * @param String $email email del usuario
	 * @param String $password contraseña del usuario
	 * @return Boolean true
	 */
	function actualizar_password($email, $password)
	{
		$array = array(
			'password' => password_hash($password, PASSWORD_DEFAULT),
			'token' => '',
			'estado' => '1'
		);

		$this->db->where('email', $email);
		$this->db->update('usuarios', $array);

		return TRUE;
	}

}