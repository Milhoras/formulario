<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_modelo extends CI_Model
{	
	function __construct()
	{
		parent::__construct();
		$this->__tabla = 'usuarios';
		$this->load->library('form_validation');
	}

	/*
	 * @access private
	 * @return Object Usuarios
	*/
	function get_all()
	{
		$this->db->from($this->__tabla);
		$this->db->where('estado', '1');

		return $this->db->get()->result();
	}

	/*
     * @access private
     * @param String email correo electrónico del usuario
     * @return Object Usuarios
    */
	function get_by_email($email)
	{
		$this->db->from($this->__tabla);
		$this->db->where('estado', '1');
		$this->db->where('email', $email);

		return $this->db->get()->row();
	}

	/*
     * @access private
     * @return int cantidad de filas afectadas
    */
	function procesar_usuario()
	{
		$this->form_validation->set_rules('nombres', 'Nombre', 'required|trim');
		$this->form_validation->set_rules('usuario', 'Usuario', 'required|trim');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('rolsitio_usuario', 'Roles de usuario', 'required');

		if($this->form_validation->run() == FALSE)
		{
			return FALSE;
		}
		else
		{
			$usuario_mod = $this->session->userdata('usuario_id');
			$usuario_id = $this->input->post('usuario_id', TRUE);
			$nombres = htmlspecialchars($this->input->post('nombres', TRUE));
			$usuario = htmlspecialchars($this->input->post('usuario', TRUE));
			$password = $this->input->post('password', TRUE);
			$email = $this->input->post('email', TRUE);
			$roles_sitio = $this->input->post('rolsitio_usuario', TRUE);

			$usuario_array = array(
				'nombres' => $nombres,
				'usuario' => $usuario,
				'password' => password_hash($password, PASSWORD_DEFAULT),
				'email' => $email,
				'usuario_mod' => $usuario_mod
			);

			$sitios_roles_usuarios = array();
			$roles_sitio = json_decode($roles_sitio, TRUE);

			if(empty($usuario_id))
			{
				// insertar a la tabla usuarios
				$usuario_array['usuario_fech_creacion'] = date('Y-m-d G:i:s');
				$this->db->insert('usuarios', $usuario_array);
				$new_usuario_id = $this->db->insert_id();

				foreach($roles_sitio as $sitio_id => $rol_id)
				{
					if(!empty($rol_id['categoriaId']))
					{
						foreach($rol_id['categoriaId'] as $categoria_id)
						{
							$sitios_roles_usuarios[] = array(
								'sitio_id' => $sitio_id,
								'rol_id' => (int)$rol_id['rolId'],
								'categoria_id' => (int)$categoria_id,
								'usuario_id' => $new_usuario_id,
								'registro_estado' => '1'
							);
						}
					}
					else
					{
						$sitios_roles_usuarios[] = array(
							'sitio_id' => $sitio_id,
							'rol_id' => (int)$rol_id['rolId'],
							'categoria_id' => NULL,
							'usuario_id' => $new_usuario_id,
							'registro_estado' => '1'
						);
					}
					
				}
			}
			else
			{
				// editar datos básicos
				$usuario_array['usuario_fech_update'] = date('Y-m-d G:i:s');
				$this->db->where('usuario_id', $usuario_id);
				$this->db->update('usuarios', $usuario_array);

				// eliminar viejos datos de relación sitio - rol
				$this->db->where('usuario_id', $usuario_id);
				$this->db->delete('usuario_rol_sitio');

				foreach($roles_sitio as $sitio_id => $rol_id)
				{
					if(!empty($rol_id['cids']))
					{
						foreach($rol_id['cids'] as $categoria_id)
						{
							$sitios_roles_usuarios[] = array(
								'sitio_id' => $sitio_id,
								'rol_id' => (int)$rol_id['rid'],
								'categoria_id' => (int)$categoria_id,
								'usuario_id' => $usuario_id,
								'registro_estado' => '1'
							);
						}
					}
					else
					{
						$sitios_roles_usuarios[] = array(
							'sitio_id' => $sitio_id,
							'rol_id' => (int)$rol_id['rid'],
							'categoria_id' => NULL,
							'usuario_id' => $usuario_id,
							'registro_estado' => '1'
						);
					}
				}
			}
			
			// insertar a la tabla sitios_roles_usuarios
			$this->db->insert_batch('usuario_rol_sitio', $sitios_roles_usuarios);
			
			return $this->db->affected_rows();
		}
	}

	/*
     * @access private
     * @param int usuario_id id del usuario
     * @return Object Usuarios
    */
	function delete_usuario($usuario_id)
	{
		$usuario_mod = $this->session->userdata('usuario_id');
		$array = array(
			'estado' => '0',
			'usuario_fech_update' => date('Y-m-d G:i:s'),
			'usuario_mod' => $usuario_mod
		);

		$this->db->where('usuario_id', $usuario_id);
		$this->db->update('usuarios', $array);

		// borrar de la tabla usuario_rol_sitio
		$this->db->where('usuario_id', $usuario_id);
		$this->db->delete('usuario_rol_sitio');

		return $this->db->affected_rows();
	}

	/*
	 * @access private
	 * @param int $id_tipo_mecanica id del tipo de mecánica
	 * @return Array $resultado
	 */
	function get_usuario_by_id($usuario_id)
	{
		// conseguir datos básicos
		$this->db->from($this->__tabla);
		$this->db->where('usuario_id', $usuario_id);
		$usuario = $this->db->get()->row();

		// conseguir sitios y roles
		$this->db->select('sitios.sitio_nombre, roles.rol_nombre, urs.sitio_id, urs.rol_id, urs.categoria_id');
		$this->db->from('usuario_rol_sitio urs');
		// $this->db->join('categorias c', 'c.categoria_id = urs.categoria_id');
		$this->db->join('sitios', 'sitios.sitio_id = urs.sitio_id');
		$this->db->join('roles', 'roles.rol_id = urs.rol_id');
		$this->db->where('usuario_id', $usuario_id);
		$roles_sitios = $this->db->get()->result();

		$resultado = array(
			'usuario' => $usuario,
			'roles_sitios' => $roles_sitios
		);

		return $resultado;
	}

	function get_usuario_for_moderador($sitios_id)
	{
		$this->db->from('usuarios');
		$this->db->join('usuario_rol_sitio urs', 'urs.usuario_id = usuarios.usuario_id');
		$this->db->where_in('urs.sitio_id', $sitios_id);
		$this->db->where('urs.rol_id !=', 1);
		$this->db->where('usuarios.estado', '1');
		$this->db->group_by('usuarios.usuario_id');
		return $this->db->get()->result();
	}

	function get_usuario_id_by_name($usuario)
	{
		$this->db->select('usuario_id');
		$this->db->from($this->__tabla);
		$this->db->where('nombres', $usuario);
		$this->db->where('estado', '1');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 )
		{
			$row = $query->row();
			return $row->usuario_id;
		}
		return false;
	}

}