<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="apple-touch-icon" sizes="57x57" href="/assets/icon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/assets/icon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/assets/icon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/icon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/assets/icon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/assets/icon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/assets/icon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/assets/icon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/icon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/assets/icon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/assets/icon/manifest.json">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <title>Thor CMS</title>

  <link rel="stylesheet" href="/assets/lib/fontawesome/css/font-awesome.css">
  <link rel="stylesheet" href="/assets/lib/weather-icons/css/weather-icons.css">
  <link rel="stylesheet" href="/assets/lib/jquery-toggles/toggles-full.css">

  <link rel="stylesheet" href="/assets/css/quirk.css">

  <script src="/assets/lib/modernizr/modernizr.js"></script>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets/lib/html5shiv/html5shiv.js"></script>
  <script src="/assets/lib/respond/respond.src.js"></script>
  <![endif]-->

 	<!-- Grocery CRUD -->
  <?php 
  if($this->session->flashdata('error') == ''):
    foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; 
  endif; ?>

</head>

<body>

  <?php echo Modules::run('sidebar/show_header');?>  

  <section>

    <?php echo Modules::run('sidebar/show_sidebar');?>

    <div class="mainpanel">

      <div class="contentpanel">

      <?php if($this->session->flashdata('error') != ''): ?>
      <div class="alert alert-danger">
        <?php echo $this->session->flashdata('error'); ?>
      </div>
      <?php else: ?>

        <ol class="breadcrumb breadcrumb-quirk">
          <li><a href="/sitios"><i class="fa fa-home mr5"></i> Inicio</a></li>
          <li><a href="buttons.html">Administrar Roles</a></li>
        </ol>

        <?php echo $output;?>

      </div><!-- contentpanel -->
      <?php endif; ?>
    </div><!-- mainpanel -->
</section>


<script src="/assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="/assets/lib/jquery-toggles/toggles.js"></script>

<script src="/assets/js/quirk.js"></script>

</body>
</html>

