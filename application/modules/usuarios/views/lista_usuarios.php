<?php 
$titulo = "Administración de Usuarios | GOL PERU";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/jqueryui/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/datatables-net/datatables.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/datatables-net.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fonts.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
<style>
	#example,
	#example2 {width:900px;float:left;}
	#example2 {margin-left: 0px;}
        #example,
	#example2 {width:900px;float:left;}
	#example2 {margin-left: 0px;}
        .ir-arriba {
                display:none;
                padding:15px;
                background:#0050b4;
                font-size:15px;
                color:#fff;
                cursor:pointer;
                position: fixed;
                bottom:20px;
                right:20px;
                border-radius: 2px;
                z-index: 1;
	</style>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
          <span class="ir-arriba icon-circle-up btn-primary"></span>  
      <header class="section-header">
        <div class="tbl">
          <div class="tbl-row">
            <div class="tbl-cell">
              <h2>Administrador de Usuarios</h2>
              <div class="subtitle">En este panel podrás administrar usuarios</div>
            </div>
          </div>
          <br>
          <div class="tbl-row">
            <div class="tbl-cell">
              <a href="<?php echo base_url();?>usuarios/crear" class="btn btn-rounded btn-inline">Crear Usuario</a>
            </div>
          </div>
        </div>
      </header>
      <section class="card">
        <div class="card-block">
          <form name='materia'>
          <table id="example" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>Email</th>
              <th>Creación</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>Email</th>
              <th>Creación</th>
              <th>Acciones</th>
            </tr>
            </tfoot>
            <tbody id='consulta'>
            <?php if(!empty($usuarios)):?>
            <?php foreach($usuarios as $usuario):?>
            <tr id="a<?php echo $usuario->usuario_id;?>">
              <td><?php echo $usuario->usuario_id;?></td>
              <td><?php echo $usuario->usuario_nombre;?></td>
              <td><?php echo $usuario->usuario_user;?></td>
              <td><?php echo $usuario->usuario_email;?></td>
              <td><?php echo $usuario->usuario_fech_creacion;?></td>
              <td>
                <a href="<?php echo base_url();?>usuarios/editar/<?php echo $usuario->usuario_id;?>"><span class="fa fa-edit"></span></a>
                &nbsp;
                <a class="eliminarUsuario" href="<?php echo base_url();?>usuarios/eliminar/<?php echo $usuario->usuario_id;?>"><span class="fa fa-close"></span></a>
              </td>
            </tr>
            <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
          </table>
          </form>
        </div>
      </section>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/datatables-net/datatables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.tablednd.0.7.min.js"></script>

<script>
  $(function() {
    $('#consulta').tableDnD();
    $('#example').DataTable({
      responsive: true,
      "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
      }
    });
    $('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});
 
	$(window).scroll(function(){
		if( $(this).scrollTop() > 0 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
  });
</script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>
</body>
</html>
