<?php 
$titulo = "Editar usuario | GOL PERU";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="box-typical box-typical-padding">
          <h5 class="m-t-lg with-border">Editar usuario</h5>

          <form action="<?php echo base_url();?>usuarios/procesar" method="post">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nombre *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="usuario_nombre" value="<?php echo $usuario['usuario']->usuario_nombre;?>" placeholder="Nombre" required></p>
              </div>
            </div>

            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 form-control-label">Contraseña *</label>
              <div class="col-sm-10">
                <input type="password" class="form-control" name="usuario_password" placeholder="Contraseña" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Usuario *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="usuario_user" placeholder="Usuario" value="<?php echo $usuario['usuario']->usuario_user;?>"  required></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Email *</label>
              <div class="col-sm-10">
                <p class="form-control-static"><input type="text" class="form-control" name="usuario_email" placeholder="Correo electrónico" value="<?php echo $usuario['usuario']->usuario_email;?>"  required></p>
              </div>
            </div>
            
            <div class="form-group row">
              <div class="col-sm-2">Asignar Rol *</div>
              <div class="col-sm-10">
                <div class="table-responsive">
                  <table class="table table-hover nomargin" id="relacionRol">
                    <thead>
                      <tr>
                        <th>Sitio</th>
                        <th>Rol</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1; foreach($usuario['roles_sitios'] as $rs):?>
                        <tr class="table-row">
                          <td class="sitio_id" data-sitio-id=<?php echo $rs[0]->sitio_id;?> ><?php echo $rs[0]->sitio_nombre;?></td>
                          <td class="rol_id" data-rol-id=<?php echo $rs[0]->rol_id;?> ><?php echo $rs[0]->rol_nombre;?></td>
                          <td>
                            <a href="#" id="editRol<?php echo $i;?>" class="edit"><span class="fa fa-edit"></span></a>
                            <a href="#" id="deleteRol<?php echo $i;?>" class="del"><span class="fa fa-trash"></span></a>
                          </td>
                        </tr>
                      <?php $i++; endforeach; ?>
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
              </div>
            </div>

            <br>

            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10" id="rolSitio1">
                <div class="col-lg-4">
                  <fieldset class="form-group">
                    <label class="form-label" for="exampleInput">Sitio</label>
                    <select id="sitios1" name="sitio_id" class="form-control sitio">
                        <option value="">&nbsp;</option>
                        <?php foreach($sitios as $sitio): ?>
                          <option value="<?php echo $sitio->sitio_id;?>"><?php echo $sitio->sitio_nombre;?></option>
                        <?php endforeach; ?>
                  </select>
                  </fieldset>
                </div>

                <div class="col-lg-4">
                  <fieldset class="form-group">
                    <label class="form-label" for="exampleInputEmail1">Rol</label>
                    <select id="roles1" name="rol_id" class="form-control rol">
                        <option value="">&nbsp;</option>
                        <?php foreach($roles as $rol): ?>
                          <option value="<?php echo $rol->rol_id;?>"><?php echo $rol->rol_nombre;?></option>
                        <?php endforeach; ?>
                  </select>
                  </fieldset>
                </div>

                <div class="col-lg-4">
                  <fieldset class="form-group">
                    <label class="form-label">&nbsp;</label>
                    <button id="addRol" class="btn"><i class="fa fa-arrow-up"></i></button>
                  </fieldset>
                </div>
              </div>
            </div>
            <input type="hidden" id="rolsitio_usuario" name="rolsitio_usuario" required>
            <input type="hidden" name="usuario_id" value="<?php echo $usuario['usuario']->usuario_id;?>" required>
            <hr>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <button id="crearUsuario" type="submit" class="btn btn-rounded btn-inline btn-primary">Editar</button>
                <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
              </div>
            </div>  
                   
          </form>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script>
$(document).ready(function(){

  // deshabilitar roles
  $('#roles1').attr('disabled', 'disabled');

  // Load hidden input
  var rolesSitios = {};
  var tabla = $("#relacionRol").children('tbody').children();
  $.each(tabla, function(index, value) {
    var sid = $(value).children("td:nth-child(1)").data('sitioId');
    var rid = $(value).children("td:nth-child(2)").data('rolId');
    rolesSitios[sid] = { rid };
  });
  $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));

  // on change SITIO
  // habilitar roles cuando ya se haya elegido un sitio
  $('#sitios1').change(function(){
    if($('#sitios1').val() != ''){
      $('#roles1').removeAttr('disabled');
      $('#roles1').find('option').remove().end();
      $("#roles1").append('<option value="">&nbsp;</option').val('');
      var data = <?php echo json_encode($roles);?>;
       $.each(data, function(i, d){
         $("#roles1").append('<option value="'+ d.rol_id + '">'
                              + d.rol_nombre + '</option>', false);
        });
    }
  });

  // Add Rol
  var tabla2 = $("#relacionRol").children('tbody').children();
  var i = tabla2.length + 1;
  
  $('#addRol').click(function(e){
    e.preventDefault();
    var newTabla = $("#relacionRol").children('tbody').children();
    var parent = $('#rolSitio1');

    var rolId = parent.children().find('.rol').val();
    var rolNombre = parent.children().find('.rol option[value="'+rolId+'"]').text();
    var sitioId = parent.children().find('.sitio').val();
    var sitioNombre = parent.children().find('.sitio option[value="'+sitioId+'"]').text();
    
    // verificar si sitio_id ya existe
    delete rolesSitios[sitioId];
    $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));
    // borrar el sitio en la tabla, loopear por la tabla y borrar el sitio id que es igual
    var editRol_id = '';
    $.each(newTabla, function(index, value) {
      var sid = $(value).children("td:nth-child(1)").data('sitioId');
      if(sid == sitioId) {
        editRol_id = $(value).children("td:nth-child(3)").children().first().attr('id');
       /* $(value).trigger('testEvent', value);
        return false;*/
        $(value).remove();
      }
    });

    if(editRol_id != ''){
      // el rol está siendo editado
      var num = parseInt(editRol_id.match(/\d+/),10);
      if(rolId && sitioId){
         $('#relacionRol').append('<tr class="table-row"><td class="sitio_id" data-sitio-id='+sitioId+'>'+sitioNombre+'</td><td data-rol-id='+rolId+'>'+rolNombre+'</td><td><a href="#" id="editRol'+num+'" class="edit"><span class="fa fa-edit"></span></a> <a href="#" id="deleteRol'+num+'" class="del"><span class="fa fa-trash"></span></a></td></tr>');
         parent.children().find('.rol').val('');
         parent.children().find('.sitio').val('');
         rolesSitios[sitioId] = {"rid":rolId};
         rolId = null;
         sitioId = null;
         $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));
      } 
    } else {
      // es un rol nuevo
      if(rolId && sitioId){
         $('#relacionRol').append('<tr class="table-row"><td class="sitio_id" data-sitio-id='+sitioId+'>'+sitioNombre+'</td><td data-rol-id='+rolId+'>'+rolNombre+'</td><td><a href="#" id="editRol'+i+'" class="edit"><span class="fa fa-edit"></span></a> <a href="#" id="deleteRol'+i+'" class="del"><span class="fa fa-trash"></span></a></td></tr>');
         parent.children().find('.rol').val('');
         parent.children().find('.sitio').val('');
         rolesSitios[sitioId] = {"rid":rolId};
         rolId = null;
         sitioId = null;
         i++;

         $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));
      } 
      i++;
    }
        
  });

  // Edit Rol
  $(document).on('click', '.edit', function(e){
    e.preventDefault();
    var hrefId = e.target.parentElement.id;
    var etiqueta = document.getElementById(hrefId);
    var sitioId = $(etiqueta).closest('tr').children().first().data('sitio-id');
    var rolId = $(etiqueta).closest('tr').children("td:nth-child(2)").data('rol-id');

    $('#sitios1').val(sitioId).change();
    $('#roles1').val(rolId).change();
  });

  // Delete Rol
  $(document).on('click', '.del', function(e){
    e.preventDefault();
    var aId = e.target.parentElement.id;
    var tag = document.getElementById(aId);
    var sitioIdKey = $(tag).closest('tr').children().first().data('sitio-id');

    delete rolesSitios[sitioIdKey];
    $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));
    tag.closest('tr').remove();

  });

  $('#crearUsuario').click(function(e){
    var rolUsuario = $('#rolsitio_usuario').val();
    if(rolUsuario == '' || rolUsuario == '{}')
    {     
      e.preventDefault();
      alert('Debes asignar un rol al usuario');
    }
  });

  $('#cancelar').click(function(e){
    e.preventDefault();
    window.location.href="<?php echo base_url();?>usuarios";
  });


});
</script>

</body>
</html>
