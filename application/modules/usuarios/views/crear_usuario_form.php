<?php 
$titulo = "Crear jugador | GOL PERU";
echo Modules::run("template/show_header", $titulo);?>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/separate/vendor/bootstrap-touchspin.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/font-awesome/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/estilos.css">
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/style.css">
</head>

<body class="with-side-menu control-panel control-panel-compact">
  <?php echo Modules::run('template/show_banner');?>
  <div class="mobile-menu-left-overlay"></div>  
  <?php echo Modules::run('template/show_sidebar');?>

  <div class="page-content">
      <div class="container-fluid">
        <div class="box-typical box-typical-padding">
          <h5 class="m-t-lg with-border">Crear usuario</h5>

          <form action="<?php echo base_url();?>usuarios/procesar" method="post" id="f1">
            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Nombre</label>
              <div class="col-sm-10">
                  <p class="form-control-static"><input type="text" class="form-control" name="usuario_nombre" id="usuario_nombre" placeholder="Nombre" required></p>
              </div>
            </div>

            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 form-control-label">Contraseña</label>
              <div class="col-sm-10">
                  <input type="password" class="form-control" name="usuario_password" id="usuario_password" placeholder="Contraseña" required>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Usuario</label>
              <div class="col-sm-10">
                  <p class="form-control-static"><input type="text" class="form-control" name="usuario_user" id="usuario_user" placeholder="Usuario" required></p>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-sm-2 form-control-label">Email</label>
              <div class="col-sm-10">
                  <p class="form-control-static"><input type="text" class="form-control" name="usuario_email" id="usuario_email" placeholder="Correo electrónico" required></p>
              </div>
            </div>
            
            <div class="form-group row">
              <div class="col-sm-2">Asignar Rol</div>
              <div class="col-sm-10">
                <div class="table-responsive">
                  <table class="table table-hover nomargin" id="relacionRol">
                    <thead>
                      <tr>
                        <th>Sitio</th>
                        <th>Rol</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div><!-- table-responsive -->
              </div>
            </div>

            <br>

            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10" id="rolSitio1">
                <div class="col-lg-4">
                  <fieldset class="form-group">
                    <label class="form-label" for="exampleInput">Sitio</label>
                    <select id="sitios1" name="sitio_id" class="form-control sitio">
                        <option value="">&nbsp;</option>
                        <?php foreach($sitios as $sitio): ?>
                          <option value="<?php echo $sitio->sitio_id;?>"><?php echo $sitio->sitio_nombre;?></option>
                        <?php endforeach; ?>
                  </select>
                  </fieldset>
                </div>

                <div class="col-lg-4">
                  <fieldset class="form-group">
                    <label class="form-label" for="exampleInputEmail1">Rol</label>
                    <select id="roles1" name="rol_id" class="form-control rol">
                        <option value="">&nbsp;</option>
                        <?php foreach($roles as $rol): ?>
                          <option value="<?php echo $rol->rol_id;?>"><?php echo $rol->rol_nombre;?></option>
                        <?php endforeach; ?>
                  </select>
                  </fieldset>
                </div>

                <div class="col-lg-4">
                  <fieldset class="form-group">
                    <label class="form-label">&nbsp;</label>
                    <button id="addRol" class="btn"><i class="fa fa-arrow-up"></i></button>
                  </fieldset>
                </div>
              </div>
            </div>
            <input type="hidden" id="rolsitio_usuario" name="rolsitio_usuario" required>
            <hr>
            <div class="form-group row">
              <div class="col-sm-2"></div>
              <div class="col-sm-10">
                <button id="crearUsuario" type="submit" class="btn btn-rounded btn-inline btn-primary">Crear</button>
                <button type="reset" id="cancelar" class="btn btn-rounded btn-inline btn-secondary-outline">Cancelar</button>
              </div>
            </div>  
                   
          </form>
        </div>
      </div><!--.container-fluid-->
  </div><!--.page-content-->

<script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/jquery/validCampoMarlon.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/js/lib/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="<?php echo base_url();?>assets/js/app.js"></script>

<script>
$(document).ready(function(){
    //Para escribir solo letras
    $('#usuario_nombre').validCampoMarlon(' abcdefghijklmnñopqrstuvwxyzáéiou');
  // deshabilitar roles
  $('#roles1').attr('disabled', 'disabled');

  // habilitar roles cuando ya se haya elegido un sitio
  $('#sitios1').change(function(){
    if($('#sitios1').val() != ''){
      $('#roles1').removeAttr('disabled');
      $('#roles1').find('option').remove().end();
      $("#roles1").append('<option value="">&nbsp;</option').val('');
      var data = <?php echo json_encode($roles);?>;
       $.each(data, function(i, d){
         $("#roles1").append('<option value="'+ d.rol_id + '">'
                              + d.rol_nombre + '</option>', false);
        });
    }
  });

  // Add Rol
  var rolesSitios = {};
  var i = 1;
  $('#addRol').click(function(e){
    e.preventDefault();
    var parent = $('#rolSitio1');
    var rolId = parent.children().find('.rol').val();
    var rolNombre = parent.children().find('.rol option[value="'+rolId+'"]').text();
    var sitioId = parent.children().find('.sitio').val();
    var sitioNombre = parent.children().find('.sitio option[value="'+sitioId+'"]').text();
    
    // var categoriaNombre = parent.children().find('.categoria option[value="'+categoriaId+'"]').text();

    $('#relacionRol').append('<tr><td class="sitio_id" data-sitio-id='+sitioId+'>'+sitioNombre+'</td><td>'+rolNombre+'</td><td><ul class="table-options"><li><a href="#" id="deleteRol'+i+'" class="btn del"><i class="fa fa-trash"></i></a></li></ul></td></tr>');
    parent.children().find('.rol').val('');
    parent.children().find('.sitio').val('');
    rolesSitios[sitioId] = {rolId};
    rolId = null;
    sitioId = null;
    i++;

    $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));
  });

  // Delete Rol
  $(document).on('click', '.del', function(e){
    e.preventDefault();
    var aId = e.target.parentElement.id;
    var tag = document.getElementById(aId);
    var sitioIdKey = $(tag).closest('tr').children().first().data('sitio-id');
    delete rolesSitios[sitioIdKey];
    $('#rolsitio_usuario').val(JSON.stringify(rolesSitios));
    tag.closest('tr').remove();

  });

  $('#crearUsuario').click(function(e){
      var usuario_nombre = $('#usuario_nombre').val();
       var usuario_password = $('#usuario_password').val();
       var usuario_user = $('#usuario_user').val();
    var rolUsuario = $('#rolsitio_usuario').val();
    
    if(usuario_nombre.trim()==="" || usuario_password.trim()==="" || usuario_user.trim()==="") {
    alert("El campo nombre, password y usuario es obligatorio");
    return false;
    }
    
/*var url_validate = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
if(!url_validate.test(usuario_user)){
   alert('la url asignada no es correcta');
   return false;
}*/
        
        else if(rolUsuario == '' || rolUsuario == '{}')
    {     
      e.preventDefault();
      alert('Debes asignar un rol al usuario');
    }
    else if(usuario_nombre.length<3 || usuario_nombre=='' || usuario_password.length<3 || usuario_password=='' || usuario_user.length<3 || usuario_user==''){
           alert("El nombre, password o usuario debe tener mas de 3 caracteres");
           return false;
       }
    else if($("#usuario_email").val().indexOf('@', 0) == -1 || $("#usuario_email").val().indexOf('.', 0) == -1) {
            alert('El correo electrónico introducido no es correcto.');
            return false;
        }
    
        else {
            return true;}
  });

  $('#cancelar').click(function(e){
    e.preventDefault();
    window.location.href="<?php echo base_url();?>usuarios";
  });


});
</script>

</body>
</html>
