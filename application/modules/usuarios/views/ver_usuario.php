<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <!--<link rel="shortcut icon" href="/assets/images/favicon.png" type="image/png">-->
  <link rel="apple-touch-icon" sizes="57x57" href="/assets/icon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/assets/icon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/assets/icon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/icon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/assets/icon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/assets/icon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/assets/icon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/assets/icon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/icon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="/assets/icon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="/assets/icon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/assets/icon/manifest.json">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="/mstile-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <title>Información de Usuario</title>

  <link rel="stylesheet" href="/assets/lib/fontawesome/css/font-awesome.css">
  <link rel="stylesheet" href="/assets/lib/weather-icons/css/weather-icons.css">
  <link rel="stylesheet" href="/assets/lib/jquery-toggles/toggles-full.css">

  <link rel="stylesheet" href="/assets/css/quirk.css">

  <script src="/assets/lib/modernizr/modernizr.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="/assets/lib/html5shiv/html5shiv.js"></script>
  <script src="/assets/lib/respond/respond.src.js"></script>
  <![endif]-->
</head>

<body>
<?php echo Modules::run('sidebar/show_header');?>
  <section>
  <?php echo Modules::run('sidebar/show_sidebar');?>
  <div class="mainpanel">

    <div class="contentpanel">
      <div class="contentpanel">
      <?php if($this->session->flashdata('error') != ''): ?>
        <div class="alert alert-danger">
          <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php else: ?>

      <ol class="breadcrumb breadcrumb-quirk">
        <li><a href="index.html"><i class="fa fa-home mr5"></i> Inicio</a></li>
        <li><a href="/usuarios">Usuarios</a></li>
        <li class="active">Información de usuario</li>
      </ol>

    <div class="row">
      <div class="col-md-10">
        <div class="panel">
          <div class="panel-heading">
            <h4 class="panel-title">Información de Usuario</h4>
            <p>En esta pestaña puedes ver la información del usuario.</p>
            <a href="/usuarios" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left"></i> Regresar</a>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-bordered table-hover table-inverse table-striped nomargin">
                <tbody>
                  <tr>
                    <td>Nombre: </td>
                    <td colspan="2"><?php echo $usuario['usuario']->usuario_nombre;?></td>
                  </tr>
                  <tr>
                    <td>Usuario: </td>
                    <td colspan="2"><?php echo $usuario['usuario']->usuario_user;?></td>
                  </tr>
                  <tr>
                    <td>Email: </td>
                    <td colspan="2"><?php echo $usuario['usuario']->usuario_email;?></td>
                  </tr>
                  <tr>
                    <td>Fecha de Creación: </td>
                    <td colspan="2"><?php echo $usuario['usuario']->usuario_fech_creacion;?></td>
                  </tr> 
                  <tr>
                    <td>Fecha de Actualización: </td>
                    <td colspan="2"><?php echo $usuario['usuario']->usuario_fech_update;?></td>
                  </tr>
                  <?php $num = count($usuario['roles_sitios']);
                  for($i = 0; $i < $num; $i++): ?>
                  <tr>
                    <td><?php echo $i == 0 ? 'Roles' : '';?></td>
                    <td><?php echo $usuario['roles_sitios'][$i]->sitio_nombre;?></td>
                    <td><?php echo $usuario['roles_sitios'][$i]->rol_nombre;?></td>
                  </tr>
                  <?php endfor; ?>
                </tbody>
              </table>
            </div><!-- table-responsive -->
          </div>
        </div><!-- panel -->
      </div>
    </div>

    <?php endif; ?>
    </div><!-- contentpanel -->
  </div><!-- mainpanel -->

</section>

<script src="/assets/lib/jquery/jquery.js"></script>
<script src="/assets/lib/jquery-ui/jquery-ui.js"></script>
<script src="/assets/lib/bootstrap/js/bootstrap.js"></script>
<script src="/assets/lib/jquery-toggles/toggles.js"></script>

<script src="/assets/js/quirk.js"></script>
</body>
</html>
